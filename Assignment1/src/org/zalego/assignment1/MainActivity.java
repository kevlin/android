package org.zalego.assignment1;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.view.View.OnCreateContextMenuListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends Activity {
	Button login, clear;
	EditText username, password, phoneNo;
	Spinner school;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		login = (Button) findViewById(R.id.btnlogin);
		clear = (Button) findViewById(R.id.btnclear);
		username = (EditText) findViewById(R.id.eduname);
		password = (EditText) findViewById(R.id.edpass);
		phoneNo = (EditText) findViewById(R.id.edphone);
		school = (Spinner) findViewById(R.id.spnschool);
		registerForContextMenu(clear);

		login.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String name = username.getText().toString();
				String schl = school.getSelectedItem().toString();
				if (!name.isEmpty()) {
					Toast.makeText(getBaseContext(),
							"Username:  " + name + "   school:    " + schl,
							Toast.LENGTH_SHORT).show();
					Intent intent=new Intent(getBaseContext(), Activity2.class);
					startActivity(intent);
					
				}

			}
		});
		clear.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				username.setText("");
				password.setText("");
				phoneNo.setText("");

				Log.d("CLEAR", "Button clear has been clicked");
				Toast.makeText(getBaseContext(), "Text Cleared",
						Toast.LENGTH_LONG).show();
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;

	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnclear:
			getMenuInflater().inflate(R.menu.contextmenu, menu);

			break;
			

		default:
			break;
		}

		super.onCreateContextMenu(menu, v, menuInfo);
	}
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		
		switch (item.getItemId()) {
		case R.id.mecopy:
			Toast.makeText(getBaseContext(), "Implement the copy fuction here", Toast.LENGTH_SHORT).show();
			
			break;
		case R.id.medelete:
			Toast.makeText(getBaseContext(), "Implement delete method here", Toast.LENGTH_SHORT).show();
			break;
		case R.id.mepaste:
			Toast.makeText(getBaseContext(), "Implement or call the paste method here", Toast.LENGTH_SHORT).show();
			break;
		case R.id.merename:
			Toast.makeText(getBaseContext(), "Implement or call the rename functionality here", Toast.LENGTH_SHORT).show();
            break;
		default:
			break;
		}
		return super.onContextItemSelected(item);
	}
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.menabt:
			Toast.makeText(getBaseContext(), "This is a tute application",
					Toast.LENGTH_LONG).show();
			break;
		case R.id.menfeedb:
			Toast.makeText(getBaseContext(),
					"You can as well give your feed back", Toast.LENGTH_LONG)
					.show();
			break;
		case R.id.menrefsh:
			Toast.makeText(getBaseContext(),
					"Refreshing the application.......", Toast.LENGTH_LONG)
					.show();
			break;
		case R.id.action_settings:
			Toast.makeText(getBaseContext(),
					"So what do you want to set here seriously?",
					Toast.LENGTH_LONG).show();
			break;

		default:
			break;
		}

		return super.onOptionsItemSelected(item);

	}

}
