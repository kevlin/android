package org.zalego.assignment3;

import android.os.Bundle;
import android.app.Activity;
import android.app.TabActivity;
import android.content.Intent;
import android.view.Menu;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

public class Admin extends TabActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_admin);
		TabHost host=getTabHost();
		TabSpec spec1=host.newTabSpec("Spec1tag");
		spec1.setIndicator("All students");
		Intent spec1in=new Intent(this,Admin1.class);
		spec1.setContent(spec1in);
		
		
		TabSpec spec2=host.newTabSpec("Spec2tag");
		spec2.setIndicator("All courses");
		Intent spec2in=new Intent(this,Admin2.class);
		spec2.setContent(spec2in);
		
		
		TabSpec spec3=host.newTabSpec("spec3tag");
		spec3.setIndicator("Details");
		Intent spec3in=new Intent(this,Admin3.class);
		spec3.setContent(spec3in);
		host.addTab(spec1);
		host.addTab(spec2);
		host.addTab(spec3);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.admin, menu);
		return true;
	}

}
