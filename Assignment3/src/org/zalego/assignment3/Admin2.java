package org.zalego.assignment3;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class Admin2 extends ListActivity {

	SQLiteDatabase mydb;
	Cursor c;
	ArrayList<String> source;
	ArrayAdapter<String> myadapter;
	ListView mylist;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_admin2);
		mydb=openOrCreateDatabase("assignment", SQLiteDatabase.CREATE_IF_NECESSARY, null);
		c=mydb.query("students", null, null, null, null, null, null);
		source=new ArrayList<String>();
		
		while (c.moveToNext()) {
			
			String course=c.getString(c.getColumnIndex("course"));
			
			source.add(course);
			
			
		}
		c.close();
		mydb.close();
		myadapter=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,source);
		
		mylist=getListView();
		mylist.setAdapter(myadapter);
		registerForContextMenu(mylist);
		
	}

	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.admin2, menu);
		return true;
	}
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		// TODO Auto-generated method stub
		getMenuInflater().inflate(R.menu.admin2, menu);
		
		
		super.onCreateContextMenu(menu, v, menuInfo);
	}
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.cmedit:
			//Toast.makeText(getBaseContext(), "Are you sure you want to edit this course!", Toast.LENGTH_SHORT).show();
			Intent intent=new Intent(getApplicationContext(),Updatecourse.class);
			startActivity(intent);
			break;
		case R.id.cmdelete:
			Toast.makeText(getBaseContext(), "Are you sure you want to delete this..", Toast.LENGTH_SHORT).show();

		default:
			break;
		}
		return super.onContextItemSelected(item);
	}

}
