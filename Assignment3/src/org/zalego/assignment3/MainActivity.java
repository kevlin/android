package org.zalego.assignment3;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
	Button signup, login;
	EditText username, password;
	Cursor c;
SQLiteDatabase mydb;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mydb= openOrCreateDatabase("assignment", SQLiteDatabase.CREATE_IF_NECESSARY	, null);
		String createtable= "CREATE TABLE IF NOT EXISTS students(username TEXT(40), password TEXT(50), course TEXT(50), school TEXT(120) )";
		mydb.execSQL(createtable);
		signup=(Button) findViewById(R.id.btnsignup);
		login= (Button) findViewById(R.id.btnlogin);
		username=(EditText) findViewById(R.id.edusername);
		password=(EditText) findViewById(R.id.edpassword);
		registerForContextMenu(login);
		login.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String user= username.getText().toString();
				String pass=password.getText().toString();
				if (!user.isEmpty() && !pass.isEmpty()) {
					
					
					//code to be executed goes in here..... call the student class
					c=mydb.query("students", null, null, null, null, null, null);
					c.moveToFirst();
					while(c.moveToNext()){
						String xuser= c.getString(c.getColumnIndex("username"));
						String xpass= c.getString(c.getColumnIndex("password"));
						if (xpass.equals(pass)&& xuser.equals(user)) {
							Intent ini= new  Intent(getApplicationContext(), Student.class);
							startActivity(ini);	
						}
						else{
							Toast.makeText(getBaseContext(), "Check your password and username for: "+ user, Toast.LENGTH_SHORT).show();
							//c.close();
						}
					}
					
					username.setText("");
					password.setText("");
						}
				
				
			}
		});
		signup.setOnClickListener( new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intt=new Intent(getApplicationContext(), Signup.class);
				startActivity(intt);
			}
		});
		
	}

	
@Override
public void onCreateContextMenu(ContextMenu menu, View v,
		ContextMenuInfo menuInfo) {
	// TODO Auto-generated method stub
	switch (v.getId()) {
	case R.id.btnlogin:
		getMenuInflater().inflate(R.menu.main, menu);
		break;

	default:
		break;
	}
	super.onCreateContextMenu(menu, v, menuInfo);
}


@Override
public boolean onContextItemSelected(MenuItem item) {
	// TODO Auto-generated method stub
	switch (item.getItemId()) {
	case R.id.cmstudent:
		Intent intent=new Intent(getApplicationContext(), MainActivity.class);
		startActivity(intent);
		
		break;
	case R.id.cmadmin:
		//code goes in here.................................................................
		Intent iin=new Intent(getApplicationContext(), Admin.class);
		startActivity(iin);
		break;
	default:
		break;
	}
	return super.onContextItemSelected(item);
}

}
