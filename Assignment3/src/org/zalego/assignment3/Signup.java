package org.zalego.assignment3;

import android.os.Bundle;
import android.app.Activity;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class Signup extends Activity {
	Button save;
	EditText user, pass, course;
	Spinner school;
	SQLiteDatabase mydb;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mydb= openOrCreateDatabase("assignment", SQLiteDatabase.CREATE_IF_NECESSARY	, null);
		String createtable= "CREATE TABLE IF NOT EXISTS students(username TEXT(40), password TEXT(50), course TEXT(50), school TEXT(120) )";
		mydb.execSQL(createtable);
		setContentView(R.layout.activity_signup);
		save = (Button) findViewById(R.id.btnsign);
		user=(EditText) findViewById(R.id.edsiguser);
		pass= (EditText) findViewById(R.id.edsignpass);
		course= (EditText) findViewById(R.id.edtcourse);
		school= (Spinner) findViewById(R.id.spschool);
		save.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				String userr=user.getText().toString();
				String passs=pass.getText().toString();
				String coursee=course.getText().toString();
				String schooll= school.getSelectedItem().toString();
				ContentValues values= new ContentValues();
				if (!userr.isEmpty() && !passs.isEmpty() && !coursee.isEmpty() && !schooll.isEmpty()) {
					values.put("username", userr);
					values.put("password", passs);
					values.put("course", coursee);
					values.put("school", schooll);
					mydb.insert("students", null, values);
					Toast.makeText(getBaseContext(), "Data save to the database..",Toast.LENGTH_SHORT).show();
					mydb.close();
					user.setText("");
					pass.setText("");
					course.setText("");
					
				}
				
				
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.signup, menu);
		return true;
	}

}
