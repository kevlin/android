package org.zalego.assignment3;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.app.ListActivity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class Stud1 extends ListActivity {

	
		SQLiteDatabase mydb;
		Cursor c;
		ArrayList<String> source;
		ArrayAdapter<String> myadapter;
		ListView mylist;

		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			
			mydb=openOrCreateDatabase("assignment", SQLiteDatabase.CREATE_IF_NECESSARY, null);
			c=mydb.query("students", null, null, null, null, null, null);
			source=new ArrayList<String>();
			
			while (c.moveToNext()) {
				
				String course=c.getString(c.getColumnIndex("course"));
				
				source.add(course);
				
				
			}
			c.close();
			mydb.close();
			myadapter=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,source);
			
			mylist=getListView();
			mylist.setAdapter(myadapter);
			
			
		}

		

	}
