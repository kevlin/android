package org.zalego.assignment3;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class Stud2 extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_stud2);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.stud2, menu);
		return true;
	}

}
