package org.zalego.assignment3;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

public class Student extends TabActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_student);
		TabHost host=getTabHost();
		TabSpec spec1=host.newTabSpec("Spec1tag");
		spec1.setIndicator("Course");
		Intent spec1in=new Intent(this,Stud1.class);
		spec1.setContent(spec1in);
		
		
		TabSpec spec2=host.newTabSpec("Spec2tag");
		spec2.setIndicator("Selected");
		Intent spec2in=new Intent(this,Stud2.class);
		spec2.setContent(spec2in);
		host.addTab(spec1);
		host.addTab(spec2);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.student, menu);
		return true;
	}

}
