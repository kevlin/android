package org.zalego.bundlesproj;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class Activity2 extends Activity {
	TextView data;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity2);
		data=(TextView) findViewById(R.id.tvdata);
		Bundle received= getIntent().getExtras();
		String name=received.getString("Name");
		data.setText("Name received is:     "+name);
		}
	

}
