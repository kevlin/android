package org.zalego.bundlesproj;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
	Button submit;
	EditText name;
	Bundle bundle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		submit = (Button) findViewById(R.id.btnSubmit);
		name = (EditText) findViewById(R.id.edname);

		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				bundle=new Bundle();
				String nam = name.getText().toString();
				if (!nam.equals("")) {

					bundle.putString("Name", nam);
					Toast.makeText(getBaseContext(), nam, Toast.LENGTH_SHORT)
							.show();
					Intent intent = new Intent(getBaseContext(),
							Activity2.class);
					intent.putExtras(bundle);
					startActivity(intent);

				}

			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
