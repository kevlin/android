package org.zalego.bundless;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends Activity {
	Button proceed;
	EditText fname, lname;
	RadioGroup gender;
	RadioButton gen;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
			proceed=(Button) findViewById(R.id.btnproceed);
			proceed.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					fname=(EditText) findViewById(R.id.edtfname);
					lname=(EditText) findViewById(R.id.edlname);
					gender=(RadioGroup) findViewById(R.id.rggender);
					gen=(RadioButton) findViewById(R.id.rdunspecified);
					
         String gendr = null;
					switch(gender.getCheckedRadioButtonId()){
					case R.id.rdunspecified:
						gendr=gen.getText().toString();;
						break;
					case R.id.rdmale:
						gendr="Male";
						break;
					case R.id.rdfemale:
						
						gendr="Female";
						break;
						default:
							
					    break;
						
					}
					
					String fisrt=fname.getText().toString();
					String last=lname.getText().toString();
					Intent inte=new Intent(getApplicationContext(), Second.class);
					Bundle bd=new Bundle();
					bd.putString("Fname", fisrt);
					bd.putString("lname", last);
					bd.putString("gender", gendr);
					inte.putExtras(bd);
					Toast.makeText(getBaseContext(), fisrt+"  "+last+"   "+gendr, Toast.LENGTH_SHORT).show();
					startActivity(inte);
					
					
					
				}
				
			});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
