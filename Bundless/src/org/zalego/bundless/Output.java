package org.zalego.bundless;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Output extends Activity {
	Bundle receive;
    TextView fnam, lnam,ag,gen,schl,marr;
    Button back,finish;
    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_output);
		receive=this.getIntent().getExtras();
		String fname=receive.getString("fname");
		String lname=receive.getString("lname");
		int age=receive.getInt("age");
		String school=receive.getString("schl");
		Boolean mar=receive.getBoolean("married");
		String gender=receive.getString("gender");
		Toast.makeText(getBaseContext(), fname+"  "+lname+"  "+gender+"  "+school+"  "+mar+"  "+age, Toast.LENGTH_SHORT).show();
		fnam=(TextView)findViewById(R.id.tvfname);
		fnam.setText(fname);
		lnam=(TextView) findViewById(R.id.tvlname);
		lnam.setText(lname);
		ag=(TextView) findViewById(R.id.tvage2);
		String ages=String.valueOf(age);
		
		ag.setText(ages);
		gen=(TextView) findViewById(R.id.tvgender);
		gen.setText(gender);
		schl=(TextView) findViewById(R.id.tvschool);
		schl.setText(school);
		String marrr;
		if (mar==true){
			marrr="Married";
		}
		else{
			marrr="Single";
		}
		marr=(TextView)findViewById(R.id.tvmarrd);
		marr.setText(marrr);
		back=(Button) findViewById(R.id.btnsec);
		finish=(Button) findViewById(R.id.btnfinsh);
		back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent ini=new Intent(getApplicationContext(), Second.class);
				startActivity(ini);
				
			}
		});
		finish.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				System.exit(0);
				
			}
		});
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.output, menu);
		return true;
	}

}
