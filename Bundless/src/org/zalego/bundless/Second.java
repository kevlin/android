package org.zalego.bundless;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class Second extends Activity {
	Bundle bd, send;
	Spinner sch;
	CheckBox marital;
	EditText ag;
	Button back, proceed;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_second);
		bd=this.getIntent().getExtras();
		final String fname=bd.getString("Fname");
		final String lname=bd.getString("lname");
		final String gender=bd.getString("gender");
		sch=(Spinner) findViewById(R.id.spschl);
		marital=(CheckBox) findViewById(R.id.chbmar);
		ag=(EditText) findViewById(R.id.edage);
		back=(Button) findViewById(R.id.btnback);
		
		proceed=(Button) findViewById(R.id.btnproceed2);
		back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent in=new Intent(getBaseContext(), MainActivity.class);
				startActivity(in);
				
			}
		});
		proceed.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String schl=sch.getSelectedItem().toString();
				Boolean married=marital.isChecked();
				int age=Integer.parseInt(ag.getText().toString());
				send=new Bundle();
				send.putString("fname", fname);
				send.putString("lname", lname);
				send.putString("gender", gender);
				send.putString("schl", schl);
				send.putBoolean("married", married);
				send.putInt("age", age);
				Intent inte=new Intent(getApplicationContext(), Output.class);
				inte.putExtras(send);
				Toast.makeText(getBaseContext(), fname+"  "+lname+"  "+gender+"  "+schl+"  "+married+"  "+age, Toast.LENGTH_SHORT).show();
				startActivity(inte);
				
				
				
			}
		});
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.second, menu);
		return true;
	}

}
