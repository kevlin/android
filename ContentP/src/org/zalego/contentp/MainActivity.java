package org.zalego.contentp;

import java.util.ArrayList;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class MainActivity extends Activity {
 Button image, audio;
 ListView mylist;
 ContentResolver cr;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	//	image=(Button) findViewById(R.id.btnImages);
		//audio= (Button) findViewById(R.id.btnAudio);
		//mylist=(ListView) findViewById(R.id.mylv);
		cr=getContentResolver();
		image.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String [] projection={MediaColumns.TITLE};
				Cursor c= cr.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI	,projection , null, null,null);
				ArrayList ttle= new ArrayList<String>(); 
				while(c.moveToNext()){
					String title= c.getString(c.getColumnIndex(MediaColumns.TITLE));
					
					ttle.add(title);
				}
				ArrayAdapter<String> myadapter;
				myadapter=new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1,ttle);
				mylist.setAdapter(myadapter);
				
				
				
			}
		});
		
		
			}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
