package org.zalego.Contakts;

import java.util.ArrayList;

import org.zalego.contentproviders.R;
import org.zalego.contentproviders.R.layout;
import org.zalego.contentproviders.R.menu;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class Contactscontent extends Activity {
Button inbox,sent;
ListView mylist;
ContentResolver cr;
ArrayList< String> data= new ArrayList<String>();
ArrayAdapter<String>adapter;
Uri sentu= Uri.parse("content://sms/sent");
Uri inboxu=Uri.parse("content://sms/inbox");
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contactscontent);
		cr=getContentResolver();
		inbox=(Button) findViewById(R.id.btnInbox);
		sent=(Button) findViewById(R.id.btnsent);
		mylist= (ListView) findViewById(R.id.smslist);
		
		inbox.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				data.clear();
				// TODO Auto-generated method stub
				Cursor c= cr.query(inboxu, null, null, null, null);
				while(c.moveToNext()){
					String body=c.getString(c.getColumnIndex("Body"));
					String Number=c.getString(c.getColumnIndex("Address"));
					 String al=body+"  "+Number;
					 data.add(al);
				}
				
				adapter=new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1,data);
				mylist.setAdapter(adapter);
			}
		});
		sent.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				data.clear();
				// TODO Auto-generated method stub
				Cursor c= cr.query(sentu, null, null, null, null);
				while(c.moveToNext()){
					String body=c.getString(c.getColumnIndex("Body"));
					String Number=c.getString(c.getColumnIndex("Address"));
					 String al=body+"  "+Number;
					 data.add(al);
				}
				
				adapter=new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1,data);
				mylist.setAdapter(adapter);
			}
		});
				
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.contactscontent, menu);
		return true;
	}

}
