package org.zalego.calllog;

import java.util.ArrayList;

import org.zalego.Contakts.Contactscontent;
import org.zalego.contentproviders.R;
import org.zalego.contentproviders.R.id;
import org.zalego.contentproviders.R.layout;
import org.zalego.contentproviders.R.menu;

import android.os.Bundle;
import android.provider.CallLog.Calls;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
//import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Data;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class CallLog extends Activity {
	Button dialed, missed, received, contacts,sms;
	ListView mylist;
	ContentResolver cr;
	String[] projection = { Calls.DATE, Calls.NUMBER, Calls.CACHED_NAME,
			Calls.DURATION };
	String dialled_calls = Integer.toString(Calls.OUTGOING_TYPE);
	String incoming = Integer.toString(Calls.INCOMING_TYPE);
	String missed_calls = Integer.toString(Calls.MISSED_TYPE);
	ArrayAdapter<String> myadpter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_call_log);
		dialed = (Button) findViewById(R.id.btndialed);
		missed = (Button) findViewById(R.id.missed);
		mylist = (ListView) findViewById(R.id.mylv);
		received = (Button) findViewById(R.id.btnreceived);
		contacts = (Button) findViewById(R.id.btncontacts);
		sms=(Button) findViewById(R.id.btnsms);
		cr = getContentResolver();
		dialed.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ArrayList<String> listcont = new ArrayList<String>();
				Toast.makeText(getBaseContext(), "You clicked Dialed",
						Toast.LENGTH_SHORT).show();
				Cursor c = cr.query(android.provider.CallLog.Calls.CONTENT_URI,
						projection, Calls.TYPE + "=?",
						new String[] { dialled_calls }, null);
				while (c.moveToNext()) {
					String date = c.getString(c.getColumnIndex(Calls.DATE));
					String No = c.getString(c.getColumnIndex(Calls.NUMBER));
					String name = c.getString(c
							.getColumnIndex(Calls.CACHED_NAME));
					String duration = c.getString(c
							.getColumnIndex(Calls.DURATION));
					String all = date + "   " + No + "   " + name + "   "
							+ duration;
					listcont.add(all);
				}

				myadpter = new ArrayAdapter<String>(getBaseContext(),
						android.R.layout.simple_list_item_1, listcont);
				mylist.setAdapter(myadpter);
			}
		});
		missed.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getBaseContext(), "You clicked missed calls",
						Toast.LENGTH_SHORT).show();
				ArrayList<String> listcont = new ArrayList<String>();

				Cursor c = cr.query(android.provider.CallLog.Calls.CONTENT_URI,
						projection, Calls.TYPE + "=?",
						new String[] { missed_calls }, null);
				while (c.moveToNext()) {
					String date = c.getString(c.getColumnIndex(Calls.DATE));
					String No = c.getString(c.getColumnIndex(Calls.NUMBER));
					String name = c.getString(c
							.getColumnIndex(Calls.CACHED_NAME));
					String duration = c.getString(c
							.getColumnIndex(Calls.DURATION));
					String all = date + "   " + No + "   " + name + "   "
							+ duration;
					listcont.add(all);
				}

				myadpter = new ArrayAdapter<String>(getBaseContext(),
						android.R.layout.simple_list_item_1, listcont);
				mylist.setAdapter(myadpter);

			}
		});
		received.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Toast.makeText(getBaseContext(), "You clicked Received Calls",
						Toast.LENGTH_SHORT).show();
				ArrayList<String> listcont = new ArrayList<String>();

				Cursor c = cr.query(android.provider.CallLog.Calls.CONTENT_URI,
						projection, Calls.TYPE + "=?",
						new String[] { incoming }, null);
				while (c.moveToNext()) {
					String date = c.getString(c.getColumnIndex(Calls.DATE));
					String No = c.getString(c.getColumnIndex(Calls.NUMBER));
					String name = c.getString(c
							.getColumnIndex(Calls.CACHED_NAME));
					String duration = c.getString(c
							.getColumnIndex(Calls.DURATION));
					String all = date + "   " + No + "   " + name + "   "
							+ duration;
					listcont.add(all);
				}

				myadpter = new ArrayAdapter<String>(getBaseContext(),
						android.R.layout.simple_list_item_1, listcont);
				mylist.setAdapter(myadpter);

			}
		});

		contacts.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String[] proj = { Contacts.DISPLAY_NAME, Contacts._ID };

				ArrayList<String> cont = new ArrayList<String>();
				Cursor c = cr.query(ContactsContract.Contacts.CONTENT_URI,
						proj, null, null, null);
				while (c.moveToNext()) {
					String name = c.getString(c
							.getColumnIndex(Contacts.DISPLAY_NAME));
					String cid = c.getString(c.getColumnIndex(Contacts._ID));
					String No = "mm";
					Cursor u = cr.query(Phone.CONTENT_URI, null, Data._ID
							+ "=?", new String[] { cid }, null);
					while (u.moveToNext()) {
						No = u.getString(u.getColumnIndex(Phone.NUMBER));

					}
					String al = name + "   : " + No;
					cont.add(al);
				}
				myadpter = new ArrayAdapter<String>(getBaseContext(),
						android.R.layout.simple_list_item_1, cont);
				mylist.setAdapter(myadpter);

			}
		});
		sms.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent iten=new Intent(getApplicationContext(), Contactscontent.class);
				startActivity(iten);
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.call_log, menu);
		return true;
	}

}
