package org.zalego.implicitintents;

import java.net.URISyntaxException;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {
	Button call,cam,go;
	EditText phone,urls;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		call =(Button) findViewById(R.id.btncall);
		cam=(Button) findViewById(R.id.btnpic);
		go=(Button) findViewById(R.id.btngo);
		phone=(EditText) findViewById(R.id.edNo);
		urls=(EditText) findViewById(R.id.edurl);
		call.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				String number=phone.getText().toString();
				Uri myuri=Uri.parse("tel:"+number);
				
				Intent inte=new Intent();
				inte.setAction(Intent.ACTION_CALL);
				inte.setData(myuri);
				startActivity(inte);
				
				
			}
		});
		
		
		cam.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent in=new Intent();
				in.setAction("android.media.action.IMAGE_CAPTURE");
				startActivity(in);
				
				
			}
		});
		
		go.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				String urlstring=urls.getText().toString();
				Intent intent=new Intent();
				intent.setAction(Intent.ACTION_VIEW);
				Uri r= Uri.parse("http://"+urlstring);
				intent.setData(r);
				startActivity(intent);
				
				
				
			}
		});
					
	}
	
	
	
	
	
	
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
