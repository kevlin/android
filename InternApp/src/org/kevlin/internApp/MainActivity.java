package org.kevlin.internApp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.kevlin.internApp.bin.data.Database;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;


public class MainActivity extends Activity {
	 Database mydb = new Database(getBaseContext());
	 private static AsyncHttpClient client = new AsyncHttpClient();
	 private static String url="www.sufurigroup.com/attachment/android.php";



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Log.i("Test", " Rendering the UI.....");
		Button bt= (Button) findViewById(R.id.btntriger);
		Button add= (Button) findViewById(R.id.btnmessage);
		add.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.i("TEST", "Save message btn clicked..... reading data from UI.....");
				EditText ename= (EditText)findViewById(R.id.name);
				EditText emessage= (EditText)findViewById(R.id.message);
				String  name= ename.getText().toString();
				String message= emessage.getText().toString();
				DatePicker dp=(DatePicker)findViewById(R.id.dptime);
				String time= dp.toString();
				Log.i("TEST", "data retrieved from the ui time to save it to the db...........");
				mydb.insert(name, message, time);
				Log.i("TEST", "Darta saved successifully");
				
				
				
			}
		});
		bt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try{
				int last= mydb.getlast();
				Log.i("Test", " About to get the last id.......");
				 RequestParams params = new RequestParams("last", 1);
				Log.i("Test", " The system retrieved the last message id="+1);
			downld(params);
				}
				catch(Exception er){
					Log.i("Test", " An error occured will trying to get the params  ...... "+er);
				}
			
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	private void downld(RequestParams params) {
		// TODO Auto-generated method stub
		Log.i("Test", "System is initializing the download process......params used are........... "+params);
		try{
			Log.i("Test", "Inside the try clause......... ");
		client.post(url, params, new JsonHttpResponseHandler() { 
			
			@Override
			public void onSuccess(JSONArray data) {
				// TODO Auto-generated method stub
				Log.i("Test", "System is downloading the json Array......");
				if(data.length()>0){
				for (int i=0; i < data.length(); i++){
					try{
					JSONObject m= data.getJSONObject(i);
					int id= m.getInt("id");
					String name= m.getString("name");
					String message= m.getString("message");
					String time= m.getString("time");
					Log.i("Test", "The system is adding the following to the database....."+id+"...."+name+"...."+message+"......");
					mydb.insert(id, name, message, time);
					Log.i("Test", "Database writing process  successful............");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						Log.i("Test", "An error occure on the onsuccess method... "+ e);
					}
					
				}
				}
				else{
					Log.i("Test", "There is no new messages in the queue to be downloaded.......");
				}
				
			}

			@Override
			public void onFailure(Throwable arg0, JSONArray arg1) {
				// TODO Auto-generated method stub
				Log.i("Test", "There was a failure in JSONArray at onFailure method....."+ arg0);
			}

			@Override
			public void onFailure(Throwable arg0, JSONObject arg1) {
				// TODO Auto-generated method stub
				Log.i("Test", "There was a failure in JSONObject at onFailure method....."+ arg0);
			}
			
			
			
		});
		}
		catch (Exception e){
			Log.i("Test", " Error occured while POSTing to"+url+".......params...."+params+"....error="+e);
		}
		 
		
	}
		


}
