package org.kevlin.internApp.bin.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class Database extends SQLiteOpenHelper{
	private static String DBNAME="intern";
	private static int VERSION=1;
	
	
	public Database(Context context) {
		super(context, DBNAME, null, VERSION);
		// TODO Auto-generated constructor stub
		Log.i("Test", "The system is running the Database constructor...........");
	}


	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		String sql=" CREATE TABLE IF NOT EXISTS message( id INTERGER PRIMARY KEY, name TEXT NOT NULL,message TEXT NOT NULL, time TEXT NOT NULL, read INTEGER NOT NULL);";
		db.rawQuery(sql,null);
		Log.i("Test", "Database created succesifully with the table message...........");
		db.rawQuery("INSERT INTO message VALUES(NULL, 'KEVIN KABURU', 'I am here', '0000-00-00 00:00:00', 1);", null);
		Log.i("Test", "Dummy data added to the table message..................");
	}


	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS message;");
		Log.i("Test", "Database upgraded succesifully..................");
		this.onCreate(db);
	}
	
	public int insert(int id,String name, String message, String time){
		Log.i("Test", "System trying to add new  data to the database...........");
		String statement="INSERT INTO message VALUES("+id+", '"+name+"', '"+message+"', '"+time+"', 1);";
		SQLiteDatabase db= this.getWritableDatabase();
		db.rawQuery(statement, null);
		Log.i("Test", "Database write completed @ Database.insert method...........");
		
		db.close();
		return id;
	}
	public int getlast(){
		Log.i("Test", "System retrieving the last intered id.................");
		SQLiteDatabase db= this.getWritableDatabase();
		int id=0;
		Cursor cursor;
		cursor=db.rawQuery("select id from message",null);
		cursor.moveToLast();
		id= cursor.getInt(cursor.getColumnIndex("id"));
		Log.i("Test", "process successful. last id is "+id+ " ..........");
		
		cursor.close();
		db.close();
		return id;
	}
	public void insert(String name, String message, String time){
		SQLiteDatabase db= this.getWritableDatabase();
		onCreate(db);
		Log.i("Test", "System trying to add new  data to the database    preparing sql INSERT statement...........");
		String statement="INSERT INTO message VALUES('"+name+"', '"+message+"', '"+time+"', 1);";
		
		db.execSQL(statement);
		Log.i("Test", "Database write completed @ Database.insert method...........");
		
		db.close();
		
	
	}

}
