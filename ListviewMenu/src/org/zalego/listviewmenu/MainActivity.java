package org.zalego.listviewmenu;






import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		String[] prezo ={"Kibaki","Uhuru","Moi","Kenyatta"};
		final ListView mylist= (ListView)findViewById(R.id.presidentlist);
		registerForContextMenu(mylist);
		ArrayAdapter<String> myadpter=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, prezo);
mylist.setAdapter(myadpter);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		
		return true;
	}
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.omedit:
			Intent intent=new Intent(getBaseContext(),Edit.class);
			startActivity(intent);
			break;
		case R.id.omdelete:
			Intent ntent=new Intent(getBaseContext(),Delete.class);
			startActivity(ntent);
		break;

		default:
			break;
		}

		return super.onOptionsItemSelected(item);

	}
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.presidentlist:
			getMenuInflater().inflate(R.menu.contextm, menu);

			break;
			

		default:
			break;
		}
	}
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		
		switch (item.getItemId()) {
		case R.id.omedit:
			Intent intent=new Intent(getBaseContext(), Edit.class);
			startActivity(intent);		
			
			break;
		case R.id.omdelete:
			Intent ntent=new Intent(getBaseContext(), Delete.class);
			startActivity(ntent);
		
		default:
			break;
		}
		return super.onContextItemSelected(item);
	}

}
