package org.zalego.locations;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity implements LocationListener{
LocationManager locman;
double latitu, longitude;
EditText lat, lon;
Button showmap;
String provider;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		lon = (EditText) findViewById(R.id.etlongtude);
		lat = (EditText) findViewById(R.id.etlatitude);
		showmap= (Button) findViewById(R.id.btnmap);
		showmap.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Bundle bd = new Bundle();
				bd.putDouble("lat", latitu);
				bd.putDouble("lon", longitude);
				Intent intent= new Intent(MainActivity.this, Mymap.class);
				intent.putExtras(bd);
				startActivity(intent);
			}
		});
		locman = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		Criteria criteria = new Criteria();
		criteria.setAccuracy(criteria.ACCURACY_FINE);
		criteria.setPowerRequirement(criteria.NO_REQUIREMENT);
		provider= locman.getBestProvider(criteria, true);
		Location location= locman.getLastKnownLocation(provider);
		locman.requestLocationUpdates(provider, 4, 1, this);
		latitu= location.getLatitude();
		longitude = location.getLongitude();
		
		if (location== null) {
			
			lon.setText("Longtitude Not available");
			lat.setText("Latitude not Available");
			
		}
		else {
			onLocationChanged(location);
			
			
			
		}
	}
@Override
	protected void onResume() {
		// TODO Auto-generated method stub
	
		super.onResume();
		locman.requestLocationUpdates(provider, 300, 50, this);
	}
	
	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		double latitu, longitude;
		latitu= location.getLatitude();
		longitude = location.getLongitude();
		lon.setText( longitude+"");
		lat.setText(latitu+"");
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

}
