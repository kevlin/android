package org.zalego.locations;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class Mymap extends Activity {
	 LatLng point;
	 GoogleMap map;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mymap);
		Bundle received= getIntent().getExtras();
		Double lat=received.getDouble("lat");
		Double lon=received.getDouble("lon");
		
		point = new LatLng(lat, lon);
		map = ((MapFragment)getFragmentManager().findFragmentById(R.id.map)).getMap();
		Marker marker = map.addMarker(new MarkerOptions().position(point).title("Niko Zalego!"));
		
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.mymap, menu);
		return true;
	}

}
