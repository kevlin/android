package org.kevdev.myapp;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class Myadapter extends ArrayAdapter<Messagemodel> {
	private final Context context;
    private final ArrayList<Messagemodel> itemsArrayList;

    public Myadapter(Context context, ArrayList<Messagemodel> itemsArrayList) {

        super(context, R.layout.row, itemsArrayList);

        this.context = context;
        this.itemsArrayList = itemsArrayList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // 1. Create inflater
        LayoutInflater inflater = (LayoutInflater) context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // 2. Get rowView from inflater
        View rowView = inflater.inflate(R.layout.row, parent, false);

        // 3. Get the  text views from the rowView
        TextView sender = (TextView) rowView.findViewById(R.id.tvsender);
        TextView time = (TextView) rowView.findViewById(R.id.tvtime);
        TextView message = (TextView) rowView.findViewById(R.id.tvmessage);
       // ImageView artistphoto= (ImageView)rowView.findViewById(R.id.list_image);
        

        
		// 4. Set the text for textView/imageview
       // artistphoto.setImageDrawable(R.drawable.artist);
        sender.setText(itemsArrayList.get(position).getsender());
        time.setText(itemsArrayList.get(position).gettime());
        message.setText(itemsArrayList.get(position).getmessage());

        // 5. retrn rowView
        return rowView;
    }
}