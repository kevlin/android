package org.kevdev.myapp;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.text.format.DateFormat;
import android.util.Log;

public class Mylocation extends Service implements LocationListener{
	 WakeLock  wakeLock;
	 WifiLock   wifiLock;

		@Override
		public void onCreate() {
			 PowerManager pm = (PowerManager) this
	                    .getSystemService(Context.POWER_SERVICE);

	            // acquire a WakeLock to keep the CPU running
	      wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
	                    "MyWakeLock");
	            if(!wakeLock.isHeld()){
	                wakeLock.acquire();
	            }

	            Log.i("MyLocation", "WakeLock acquired!");


	            WifiManager wm = (WifiManager) this.getSystemService(Context.WIFI_SERVICE);
	         wifiLock = wm.createWifiLock(WifiManager.WIFI_MODE_FULL , "MyWifiLock");
	            if(!wifiLock.isHeld()){
	                wifiLock.acquire();
	            }
			
			
		};
		public int onStartCommand(Intent intent, int flags, int startId) {
			// TODO Auto-generated method stub
			Log.d("Mylocation","My Location started ");
			
					locmethod();
				
			
			return super.onStartCommand(intent, flags, startId);
		}

		@Override
		public void onLocationChanged(Location location) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public IBinder onBind(Intent intent) {
			// TODO Auto-generated method stub
			return null;
		}
		public void locmethod(){
			 LocationManager locman;
						String provider;
						double latitu, longitude;
						Location location;



						if (Looper.myLooper() == null) {
					        Looper.prepare();
					    }
							
							locman = (LocationManager)this.getSystemService(this.LOCATION_SERVICE);
							Criteria criteria = new Criteria();
							criteria.setAccuracy(criteria.ACCURACY_FINE);
							criteria.setHorizontalAccuracy(criteria.ACCURACY_HIGH);
							criteria.setPowerRequirement(criteria.NO_REQUIREMENT);
							provider= locman.getBestProvider(criteria, true);
							locman.requestLocationUpdates(provider, 300, 50, this);
							location= locman.getLastKnownLocation(provider);
							locman.requestLocationUpdates(provider, 4, 1, this);
							latitu= location.getLatitude();
							longitude = location.getLongitude();
							String tim= (DateFormat.format("yyyy-MM-dd hh:mm:ss", new java.util.Date()).toString());
							
							//...............................send online.......
							HttpClient client = new DefaultHttpClient();
							HttpPost post= new HttpPost("http://playground.podserver.info/index.php");
							List<NameValuePair> data = new ArrayList<NameValuePair>();
							data.add(new BasicNameValuePair("lat", latitu+""));
							data.add(new BasicNameValuePair("lon", longitude+""));
							data.add(new BasicNameValuePair("time", tim));
							data.add(new BasicNameValuePair("owner_id", "1"));
							
							try{
							post.setEntity(new UrlEncodedFormEntity(data));
							client.execute(post);
							Log.e("locsent","At:   "+tim +"  "+ latitu+", "+longitude+" ");
							
							}
							catch (Exception e) {
								// TODO: handle exception
								Log.d("posterror", e.getMessage());
								locmethod();
							}
							
							}
		
				@Override
				public void onDestroy() {
				// TODO Auto-generated method stub
					if (wakeLock != null) {
		                if (wakeLock.isHeld()) {
		                    wakeLock.release();
		                    Log.i("Mylocation", "WakeLock released!");
		                }
		            }

		            // release the WifiLock
		            if (wifiLock != null) {
		                if (wifiLock.isHeld()) {
		                    wifiLock.release();
		                    Log.i("Mylocation", "WiFi Lock released!");
		                }
					
					
					
					
					
				super.onDestroy();
				}			
				}
		
		
	}