package com.zalego.mybradcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.widget.Toast;

public class SmsReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub

		Bundle bun= intent.getExtras();
		Object[] item= (Object[]) bun.get("pdus");
		
		
		for (int i = 0; i < item.length; i++) {
			SmsMessage sms= SmsMessage.createFromPdu((byte[]) item[i]);
			String body= sms.getMessageBody();
			String numb= sms.getOriginatingAddress();
			if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")){
				Toast.makeText(context, numb+" : sent this message '"+body+" '", Toast.LENGTH_LONG).show();
				
			}
		}
		
		
	}

}
