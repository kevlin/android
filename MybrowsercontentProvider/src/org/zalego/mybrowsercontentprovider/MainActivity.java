package org.zalego.mybrowsercontentprovider;

import java.util.ArrayList;

import android.os.Bundle;
import android.provider.Browser;
import android.app.Activity;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity {
 ContentResolver cr;
 Button settings, bookmarks;
 ListView mylist;
 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		cr=getContentResolver();
		
		settings = (Button) findViewById(R.id.settings);
		bookmarks= (Button) findViewById(R.id.bookmarks);
		mylist= (ListView) findViewById(R.id.mylist);
		bookmarks.setOnClickListener(new OnClickListener() {
		ArrayList<String> myarray;	
		ArrayAdapter<String> myadapter;
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				myarray = new ArrayList<String>(); 
				Cursor c=cr.query(Browser.BOOKMARKS_URI, null, null, null, null);
				
				while(c.moveToNext()){
					String one=c.getString(1);
					String two=c.getString(2);
					String all= one+"  "+two;
					myarray.add(all);
					
					
				}
				myadapter= new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1,myarray);;
				mylist.setAdapter(myadapter);
				
			}
		});
		settings.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				Intent intent=new Intent(getApplicationContext(), Settingscont.class);
				startActivity(intent);
			}
		});
	}

	
}
