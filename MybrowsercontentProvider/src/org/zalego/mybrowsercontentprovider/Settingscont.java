package org.zalego.mybrowsercontentprovider;

import java.util.ArrayList;

import android.os.Bundle;
import android.provider.Settings;
import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class Settingscont extends Activity {
ListView mylist;
ContentResolver cr;
ArrayList<String> myarraylist =new ArrayList<String>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		
		mylist=(ListView) findViewById(R.id.lvsettings);
		cr= getContentResolver();
		Cursor c= cr.query(Settings.System.CONTENT_URI, null, null, null, null);
		while(c.moveToNext()){
			
			String name= c.getString(c.getColumnIndex(Settings.System.NAME));
			String value= c.getString(c.getColumnIndex(Settings.System.VALUE));
			String all= name+ "  "+ value;
			myarraylist.add(all);
		}
		 ArrayAdapter<String> myadapter= new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1,myarraylist);
		 mylist.setAdapter(myadapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.settings, menu);
		return true;
	}

}
