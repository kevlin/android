package org.zalego.myservice;

import java.util.EmptyStackException;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
Button start, stop, intentservice, asyncT;
EditText number;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		start= (Button)findViewById(R.id.btnstart);
		stop= (Button)findViewById(R.id.btnstop);
		intentservice= (Button)findViewById(R.id.btnintentservice);
		asyncT= (Button)findViewById(R.id.btnasynctask);
		number=(EditText) findViewById(R.id.etbooks);
		start.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent intent= new Intent(getBaseContext(), Exampleservice.class);
				startService(intent);
				
				
			}
		});
		stop.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent= new Intent(getBaseContext(), Exampleservice.class);
				stopService(intent);
			}
		});
		intentservice.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent inten= new Intent(getBaseContext(), Myintentservice.class);	
				startService(inten);
				
				
			}
		});
		asyncT.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String no= number.getText().toString();
				if(no!=""){
					Dobackground bg= new Dobackground(no);
					bg.execute();
				}
				
			}
		});
		
		
}
	
	class Dobackground extends AsyncTask<String, String, String> {
		int number;
		public Dobackground(String number){
			int myno= Integer.parseInt(number);
			this.number= myno;
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			for (int i = 0; i < number; i++) {
				try {
					Thread.sleep(5000);
					
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					Log.e("My bg", "this is the error:  "+ e);
				}
				publishProgress(null);
			}
			
			return null;
		}

		@Override
		protected void onProgressUpdate(String... values) {
			// TODO Auto-generated method stub
			Toast.makeText(getBaseContext(), "Book finished downloading .....", Toast.LENGTH_SHORT).show();
			super.onProgressUpdate(values);
		}
		
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			Toast.makeText(getBaseContext(), "All books downloaded", Toast.LENGTH_SHORT).show();
			super.onPostExecute(result);
		}
		
		
		
	}

}
