package org.zalego.preference;

import android.os.Bundle;
import android.app.Activity;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
	SharedPreferences prefs;
	SharedPreferences.Editor editor;
	Button login, clear;
	EditText uname, pass;
	CheckBox rem;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		prefs=getSharedPreferences("kevinpref", 0);
		editor=prefs.edit();
		
		
		rem= (CheckBox)findViewById(R.id.chkrem);
		uname=(EditText)findViewById(R.id.edusername);
		pass=(EditText)findViewById(R.id.edpassword);
		login=(Button) findViewById(R.id.btnlogin);
		clear= (Button) findViewById(R.id.btnclear);
		login.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				String user=uname.getText().toString();
				String password=pass.getText().toString();
			
			
			if (rem.isChecked()) {
				
				editor.putString("username", user);
				editor.putString("password", password);
				editor.commit();
				Toast.makeText(getBaseContext(), "Username: "+ user+ "  password:  "+password , Toast.LENGTH_SHORT).show();
				
			}
			else {
				
				Toast.makeText(getBaseContext(), "User data not saved", Toast.LENGTH_SHORT).show();
					
			}
				
			}
			
			
			
			
			
			
			
			
			
			
			
		});
		
		
		clear.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				uname.setText("");
				pass.setText("");
				rem.setChecked(false);
				editor.putString("username", "");
				editor.putString("password", "");
				
				editor.commit();
			}
		});
		
		String storedU=prefs.getString("username", "");
		String storedp=prefs.getString("password", "");
		uname.setText(storedU);
		pass.setText(storedp);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
