package org.zalego.remotedb;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {
	Button submit, clear;
	EditText username, password;
	String name, pass;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		submit= (Button)findViewById(R.id.btnsubmit);
		clear = (Button)findViewById(R.id.btnclear);
		username= (EditText)findViewById(R.id.edtname);
		password= (EditText)findViewById(R.id.edtpassword);
		submit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				name = username.getText().toString();
				 pass= password.getText().toString();
				if(!name.isEmpty() && !pass.isEmpty()){
					
					//SubmitDetails sub = 
							new SubmitDetails().execute();	
					
					
							username.setText("");
							password.setText("");
				}
				
				
			}
		});
		clear.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent intent = new Intent(getApplicationContext(), Users.class);
				startActivity(intent);
				
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	
	
	class SubmitDetails extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			//........................................................................................
			HttpClient client = new DefaultHttpClient();
			HttpPost post= new HttpPost("http://192.168.1.173/class/submit.php");
			List<NameValuePair> data = new ArrayList<NameValuePair>();
			data.add(new BasicNameValuePair("name", name));
			data.add(new BasicNameValuePair("password", pass));
			try {
			post.setEntity(new UrlEncodedFormEntity(data));
			client.execute(post);
			
			}
			catch (Exception e) {
				// TODO: handle exception
				Log.d("post error", e.toString());
			}
			
			
			return null;
		}
	
	}
}
