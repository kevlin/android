package org.zalego.remotedb;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.DefaultClientConnection;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class Users extends Activity {
ListView mydata;
ProgressDialog dialog;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_users);
		mydata = (ListView) findViewById(R.id.mlist);
		new RetrievData(this).execute();
	}

	
	class RetrievData extends AsyncTask<String, String, String>{

		private String result;
		public RetrievData(Context con){
			
			dialog=new ProgressDialog(con);
			
		}
		
		@Override
		protected void onPreExecute() {
			dialog.setMessage("Loading .....");
			dialog.show();
			
			
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost("http://192.168.1.173/class/jsondata.php");
			try {
			HttpResponse response= client.execute(post);
			
			InputStream stream = response.getEntity().getContent();
			byte[] data = new byte[256];
			StringBuffer buffer = new StringBuffer();
			int length = 0;
			
			while(-1 != (length= stream.read(data))){
			String item = new String(data, 0,length);
			buffer.append(item);
				
			}
			 result = buffer.toString();
			
			
			
			
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Log.d("error", e.toString());
			}
			
			
			
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			ArrayAdapter<String> myadapter;
			ArrayList<String> mylist= new ArrayList<String>();
			try {
				JSONObject json= new JSONObject(result);
				JSONArray jarray= json.getJSONArray("item");
				for (int i = 0; i < jarray.length(); i++) {
					JSONObject job= jarray.getJSONObject(i);
					String usrname = job.getString("username");
					String pssword = job.getString("password");
					String details = usrname + "   "+pssword;
					mylist.add(details);
				}
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
  myadapter= new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1,mylist);

  mydata.setAdapter(myadapter);
  dialog.dismiss();
//			mydata.setText(result);
			
		}
		
		
		
		
		
	}

}
