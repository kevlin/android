package org.zalego.sqlitedb;

import android.os.Bundle;
import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Edit extends Activity {
EditText usern, name;
Button update;
SQLiteDatabase mydb;
String rename;
Cursor c;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit);
		
		usern= (EditText) findViewById(R.id.edddusern);
		name=(EditText) findViewById(R.id.edddname);
		update= (Button) findViewById(R.id.btnddsubmit);
		Bundle received= getIntent().getExtras();
		rename=received.getString("user");
		mydb=openOrCreateDatabase("zalego", SQLiteDatabase.CREATE_IF_NECESSARY, null);
		c=mydb.query("details", new String[]{"username","name"}, "name=?", new String[]{rename	}, null, null, null);
		while (c.moveToNext()) {
			String username= c.getString(c.getColumnIndex("username"));
			String rname=c.getString(c.getColumnIndex("name"));
			name.setText(rname);
			usern.setText(username);
		}
		c.close();
		
		
		
        
		update.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String nname=name.getText().toString();
				String nuser= usern.getText().toString();
				if (!nname.isEmpty() && !nuser.isEmpty()) {
					//updating hapens here
					ContentValues values= new ContentValues();
					values.put("username", nuser);
					values.put("name",nname);
					mydb.update("details", values, "name=?", new String[]{rename});
					Toast.makeText(getBaseContext(), "Updated  " + nname+ "  "+ nuser, Toast.LENGTH_SHORT).show();
					
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.edit, menu);
		return true;
	}
	

}
