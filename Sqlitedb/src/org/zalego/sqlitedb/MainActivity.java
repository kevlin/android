package org.zalego.sqlitedb;

import android.os.Bundle;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
	SQLiteDatabase mydb;
	Button submit, qery;
	EditText edname, edusername;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		mydb=openOrCreateDatabase("zalego", SQLiteDatabase.CREATE_IF_NECESSARY, null);
		String tablequery="CREATE TABLE IF NOT EXISTS details( name TEXT(40), username TEXT(50) )";
		mydb.execSQL(tablequery);
		
		edname=(EditText) findViewById(R.id.edname);
		edusername=(EditText) findViewById(R.id.edusern);
		qery=(Button) findViewById(R.id.btnquery);
		submit=(Button) findViewById(R.id.btnsubmit);
		submit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				String name=edname.getText().toString();
				String username= edusername.getText().toString();
				ContentValues values=new ContentValues();
				
				if (!name.isEmpty() && !username.isEmpty()) {
					values.put("name", name);
					values.put("username", username);
					mydb.insert("details",null, values);
					
					
					Toast.makeText(getBaseContext(), "Done saving to the db", Toast.LENGTH_SHORT).show();
					
				}
				else {
					Toast.makeText(getBaseContext(), "Fill all fields", Toast.LENGTH_SHORT).show();
				}
				
				
			}
		});
		
		qery.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(getApplicationContext(), Query.class);
				startActivity(intent);
				
			}
		});
				
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
