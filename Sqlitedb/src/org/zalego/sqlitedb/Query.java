package org.zalego.sqlitedb;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class Query extends ListActivity {
	SQLiteDatabase mydb;
	Cursor c;
	ArrayList<String> source;
	ArrayAdapter<String> myadapter;
	ListView mylist;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_query);
		mydb=openOrCreateDatabase("zalego", SQLiteDatabase.CREATE_IF_NECESSARY, null);
		c=mydb.query("details", null, null, null, null, null, null);
		source=new ArrayList<String>();
		
		while (c.moveToNext()) {
			
			String name=c.getString(c.getColumnIndex("name"));
			//String uname=c.getString(c.getColumnIndex("username"));
			source.add(name);
			//source.add(uname);
			
		}
		c.close();
		myadapter=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,source);
		
		mylist=getListView();
		mylist.setAdapter(myadapter);
		registerForContextMenu(mylist);
		mylist.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position,
					long id) {
				// TODO Auto-generated method stub
				String item=parent.getItemAtPosition(position).toString();
				Bundle bundle= new Bundle();
				bundle.putString("user", item);
				Intent inte=new Intent(getApplicationContext(), Edit.class);
				inte.putExtras(bundle);
				startActivity(inte);
				
				
			}
		});
		
		mylist.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> paren, View vt,
					int positio, long i) {
				// TODO Auto-generated method stub
				String del= paren.getItemAtPosition(positio).toString();
				//Toast.makeText(getBaseContext(), "I am here", Toast.LENGTH_SHORT).show();
				String sqldel="DELETE FROM details WHERE name='"+del+"'";
				mydb.execSQL(sqldel);
				
				return false;
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.query, menu);
		return true;
	}
	

}
