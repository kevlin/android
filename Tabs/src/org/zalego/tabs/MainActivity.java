package org.zalego.tabs;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.TabActivity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.Toast;
import android.widget.TabHost.TabSpec;

public class MainActivity extends TabActivity {
	EditText searc;
	Button search;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		TabHost host=getTabHost();
		TabSpec spec1=host.newTabSpec("Spec1tag");
		spec1.setIndicator("Tab One");
		Intent spec1in=new Intent(this,Activity1.class);
		spec1.setContent(spec1in);
		
		
		TabSpec spec2=host.newTabSpec("Spec2tag");
		spec2.setIndicator("Tab two");
		Intent spec2in=new Intent(this,Activity2.class);
		spec2.setContent(spec2in);
		
		
		TabSpec spec3=host.newTabSpec("spec3tag");
		spec3.setIndicator("Tab three");
		Intent spec3in=new Intent(this,Activity3.class);
		spec3.setContent(spec3in);
		host.addTab(spec1);
		host.addTab(spec2);
		host.addTab(spec3);
		
		
		
		ActionBar action=getActionBar();
		action.setTitle("Navigation");
		action.setCustomView(R.layout.actions);
		action.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM|ActionBar.DISPLAY_SHOW_HOME);
		search=(Button) action.getCustomView().findViewById(R.id.btnsearch);
		searc=(EditText)action.getCustomView().findViewById(R.id.edsearch);
		search.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String t=searc.getText().toString();
				if (!t.equals("")) {
					Toast.makeText(getBaseContext(), t, Toast.LENGTH_SHORT).show();
					
				}
				
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
 @Override
public boolean onOptionsItemSelected(MenuItem item) {
	// TODO Auto-generated method stub
	 
	 switch (item.getItemId()) {
	case R.id.mencreate:
		Toast.makeText(getBaseContext(), "Create a new user here.....", Toast.LENGTH_SHORT).show();
		
		break;
	case R.id.menRead:
		Toast.makeText(getBaseContext(), "query all users", Toast.LENGTH_SHORT).show();
		break;
	
	default:
		break;
	}
	 
	 
	return super.onOptionsItemSelected(item);
}

}
