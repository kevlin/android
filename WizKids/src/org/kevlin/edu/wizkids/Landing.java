package org.kevlin.edu.wizkids;



import java.util.Date;

import org.kevlin.edu.wizkidz.data.WizData;
import org.kevlin.edu.wizkidz.model.User;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;


public class Landing extends Activity {
WizData wiz= new WizData(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing); 
        Button save=(Button) findViewById(R.id.btnsave);
        save.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				EditText fn=(EditText) findViewById(R.id.edfname);
				EditText ln = (EditText) findViewById(R.id.edlname);
				EditText sc= (EditText) findViewById(R.id.edschl);
				EditText cls=(EditText) findViewById(R.id.edclass);
				EditText db= (EditText)findViewById(R.id.eddob);
				String fname=fn.getText().toString();
				String lname=ln.getText().toString();
				String school=sc.getText().toString();
				String clss=cls.getText().toString();
				String dob=db.getText().toString();
				Log.i("TEST", "Time to add the data t the table user...............");
				
				wiz.userInsert(new User(dob, fname, lname, school, clss));
				Log.i("TEST", "Data added to the table  USER,,,,,,,,,,,,,");
			}
		});
        Button list= (Button) findViewById(R.id.btnlist);
        list.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent intent= new Intent(getApplicationContext(), Output.class);
				startActivity(intent);
				
			}
		});
       
    
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_landing, menu);
        return true;
    }
    
}
