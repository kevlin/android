package org.kevlin.edu.wizkids;

import org.kevlin.edu.wizkidz.data.WizData;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;


public class Output extends Activity {
WizData wiz= new WizData(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.output); 
        Button read=(Button) findViewById(R.id.btntoast);
        read.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				wiz.readUser();
				Toast.makeText(getApplicationContext(), "ALL data fetched.............", Toast.LENGTH_LONG).show();
				
			}
		});
    }
}
