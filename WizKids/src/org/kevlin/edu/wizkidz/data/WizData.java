package org.kevlin.edu.wizkidz.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.kevlin.edu.wizkidz.model.Answers;
import org.kevlin.edu.wizkidz.model.Choices;
import org.kevlin.edu.wizkidz.model.Classs;
import org.kevlin.edu.wizkidz.model.Images;
import org.kevlin.edu.wizkidz.model.Language;
import org.kevlin.edu.wizkidz.model.Questions;
import org.kevlin.edu.wizkidz.model.Scores;
import org.kevlin.edu.wizkidz.model.Stories;
import org.kevlin.edu.wizkidz.model.StoryQuestions;
import org.kevlin.edu.wizkidz.model.Subjects;
import org.kevlin.edu.wizkidz.model.User;

import android.content.Context;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;



public class WizData extends SQLiteOpenHelper{
	private static final String DBNAME="wizdatabase";
	private static final int DBVERSION=2;
	
	
	//import all the table models
	Answers answers=new Answers();
	Choices choices=new Choices();
	Classs classs=new Classs();
	Images images= new Images();
	Language language=new Language();
	Questions questions=new Questions();
	Scores scores=new Scores();
	Stories stories=new Stories();
	StoryQuestions storyquestions=new StoryQuestions();
	Subjects subjects=new Subjects();
	User user=new User();

	
	 //Database constructor
	public WizData(Context context) {
		super(context, DBNAME, null, DBVERSION);
		Log.i("TEST", "I am at the constructor.............");
		
	}

	
	
	//method to create all the tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(user.createtable());
		db.execSQL(images.createtable());
		db.execSQL(choices.createtable());
		db.execSQL(classs.createtable());
		db.execSQL(language.createtable());
		db.execSQL(answers.createtable());
		db.execSQL(stories.createtable());		
		db.execSQL(subjects.createtable());
		db.execSQL(scores.createtable());
		db.execSQL(questions.createtable());
		db.execSQL(storyquestions.createtable());
		
	}

	



	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(storyquestions.droptable());
		db.execSQL(questions.droptable());		
		db.execSQL(scores.droptable());
		db.execSQL(subjects.droptable());
		db.execSQL(stories.droptable());
		db.execSQL(answers.droptable());
		db.execSQL(language.droptable());
		db.execSQL(classs.droptable());
		db.execSQL(choices.droptable());
		db.execSQL(images.droptable());
		db.execSQL(user.droptable());
		
		
		
	onCreate(db);	
	}
	
	//methods to perform all the CRUD functions to the database
	//inserts to all tables
	
	public  void answersInsert(Answers answers){
		SQLiteDatabase mdb= this.getWritableDatabase();
		try
		{
			//db.beginTransaction();
			
			//insert this row
			
			String answer= answers.getAnswer();
			int choice= answers.getChoice();
			
			String insert = "INSERT INTO answers "+" (answer,choice_id) VALUES (?,?);";
			mdb.execSQL(insert,new Object[]{answer,choice});
			
			//db.setTransactionSuccessful();
		}
		finally
		{
			//db.endTransaction();
		}
		
	}
	
public void choicesInsert(Choices choices){
	SQLiteDatabase mdb= this.getWritableDatabase();
	try
	{
		//mdb.beginTransaction();
		
		//insert this row
		
		String choice1= choices.getChoice1();
		String choice2=choices.getChoice2();
		String choice3=choices.getChoice3();
		
		String insert = "INSERT INTO choices "+" (choice1,choice2,choice3) VALUES (?,?,?);";
		mdb.execSQL(insert,new Object[]{choice1,choice2,choice3});
		
		//db.setTransactionSuccessful();
	}
	finally
	{
		//db.endTransaction();
	}	
	}

public void classsInsert(Classs classs){
	SQLiteDatabase mdb= this.getWritableDatabase();
	try
	{
		//db.beginTransaction();
		
		//insert this row
		String cls= classs.getClasss();
		int years= classs.getYears();
		
		String insert = "INSERT INTO class "+" (class,years) VALUES (?,?);";
		mdb.execSQL(insert,new Object[]{cls,years});
		
		//db.setTransactionSuccessful();
	}
	finally
	{
		//db.endTransaction();
	}	
	
}
public void imagesInsert(Images images){
	SQLiteDatabase mdb= this.getWritableDatabase();
	try
	{
		//db.beginTransaction();
		
		//insert this row
		String image= images.getName();
		
		
		String insert = "INSERT INTO images "+" (name) VALUES (?);";
		mdb.execSQL(insert,new Object[]{image});
		
		//db.setTransactionSuccessful();
	}
	finally
	{
		//db.endTransaction();
	}	
	
	
}

public void languageInsert(Language language){
	SQLiteDatabase mdb= this.getWritableDatabase();
	try
	{
		//db.beginTransaction();
		
		//insert this row
		String name= language.getName();
		
		
		String insert = "INSERT INTO language "+" (name) VALUES (?);";
		mdb.execSQL(insert,new Object[]{name});
		
		//db.setTransactionSuccessful();
	}
	finally
	{
		//db.endTransaction();
	}	
	
	
}
public void questionInsert(Questions questions){
	SQLiteDatabase mdb= this.getWritableDatabase();
	
	try
	{
		//db.beginTransaction();
		
		//insert this row
		String question= questions.getQuestion();
		int answered=questions.getAnswered();
		int marks=questions.getMarks();
		int subject=questions.getSubject();
		int img=questions.getImage();
		int answ= questions.getAnswer();
		
		String insert = "INSERT INTO questions "+" (question, answered, marks,subjects_id, images_id ,answers_id) VALUES (?,?,?,?,?,?);";
		mdb.execSQL(insert,new Object[]{question,answered,marks,subject,img,answ});
		
		//db.setTransactionSuccessful();
	}
	finally
	{
		//db.endTransaction();
	}	
	
}
public void scoresInsert(Scores scores){
	SQLiteDatabase mdb= this.getWritableDatabase();
	try
	{
		//db.beginTransaction();
		
		//insert this row
		int score= scores.getScores();
		int user= scores.getUser();
		
		String insert = "INSERT INTO scores "+" (scores, user_id) VALUES (?,?);";
		mdb.execSQL(insert,new Object[]{score,user});
		
		//db.setTransactionSuccessful();
	}
	finally
	{
		//db.endTransaction();
	}	
}
public void storiesInsert(Stories stories){
	SQLiteDatabase mdb= this.getWritableDatabase();
	try
	{
		//db.beginTransaction();
		
		//insert this row
		String title= stories.getTitle();
		String story=stories.getStory();
		int lang_id=stories.getLang();
		int img_id=stories.getImage();
		
		String insert = "INSERT INTO stories "+" (title, story, language_id, images_id) VALUES (?,?,?,?);";
		mdb.execSQL(insert,new Object[]{title,story,lang_id,img_id});
		
		//db.setTransactionSuccessful();
	}
	finally
	{
		//db.endTransaction();
	}
}
public void storyquestionsInsert(StoryQuestions storyquestions){
	SQLiteDatabase mdb= this.getWritableDatabase();
	try
	{
		//db.beginTransaction();
		
		//insert this row
		String question=storyquestions.getQuestion();
		int marks= storyquestions.getMarks();
		int answered= storyquestions.getAnswered();
		int story= storyquestions.getStory();
		int img= storyquestions.getImage();
		int answ= storyquestions.getAnswer();
		
		String insert = "INSERT INTO story_questions "+" (question, marks, answered, stories_id, images_id ,answers_id) VALUES (?,?,?,?,?,?);";
		mdb.execSQL(insert,new Object[]{question,marks,answered,story,img,answ});
		
		//db.setTransactionSuccessful();
	}
	finally
	{
		//db.endTransaction();
	}
	
}
public void subjectsInsert(Subjects subjects){
	SQLiteDatabase mdb= this.getWritableDatabase();
	try
	{
		//db.beginTransaction();
		
		//insert this row
		String sub=subjects.getSubject();
		int cls= subjects.getClasss();
		
		String insert = "INSERT INTO subjects"+" (subject, class_id) VALUES (?,?);";
		mdb.execSQL(insert,new Object[]{sub,cls});
		
		//db.setTransactionSuccessful();
	}
	finally
	{
		//db.endTransaction();
	}
}
public void userInsert(User user){
	SQLiteDatabase mdb= this.getWritableDatabase();
	Log.i("TEST", "UserInsert method running...........");
	
	try
	{
		Log.i("TEST", "bd.begintransaction about to be called.............");
	
		Log.i("TEST", "db.begintransaction called initializing data..............");
		//insert this row
		String dob= user.getDob();
		String fname= user.getFname();
		String lname= user.getLname();
		String schl= user.getSchool();
		String cls= user.getClasss();
		
		String insert = "INSERT INTO user"+" (fname,lname,dob,school, class) VALUES (?,?,?,?,?);";
		Log.i("TEST", "Time to write to  the table.....................");
		mdb.execSQL(insert,new Object[]{fname,lname,dob,schl,cls});
		
		//mdb.setTransactionSuccessful();
		Log.i("TEST", "Transaction successful..................");
	}
	finally
	{
		
		Log.i("TEST", "Transaction ended.............................");
	}
	
}


//All the read db will go in here
//Testing... dbreader
public void readUser(){
	Log.d("Table", "User");
	Log.i("TEST", "I am int the reader method..... ready to starn teading");
	String sql="select * from user";
	SQLiteDatabase mdb= this.getReadableDatabase();
	Cursor cursor= mdb.rawQuery(sql, null);
	Log.i("TEST", "Cursor initialized");
	try{
	cursor.moveToFirst();
	while(cursor.moveToNext()){
		int id=cursor.getInt(0);
		String fnam=cursor.getString(1);
		String lnam= cursor.getString(2);
		String dob=cursor.getString(3);
		String schl= cursor.getString(4);
		String clas= cursor.getString(5);
		Log.i("results:", "ID:"+id+"   FNAME:"+fnam+"    LNAME:"+lnam+"     DOB"+dob+"    SCHOOL:"+schl+"     CLASS:"+clas);
		
	}
	}
	catch (Exception e) {
		Log.i("Error:  ", e.toString());
	}
	cursor.close();
	mdb.close();
	
}
public void readsubjects(){
	Log.d("Table", "Subjects");
	Log.i("TEST", "I am int the reader method reading subjects table..... ready to start reading");
	String sql="select * from subjects";
	SQLiteDatabase mdb= this.getReadableDatabase();
	Cursor cursor= mdb.rawQuery(sql, null);
	Log.i("TEST", "Cursor initialized with data.......");
	try{
	cursor.moveToFirst();
	while(cursor.moveToNext()){
		int id=cursor.getInt(0);
		String subject=cursor.getString(1);
		String clss= cursor.getString(2);
		
		Log.i("results:", "ID:"+id+"   subject:"+subject+"    class:  "+clss);
		
	}
	}
	catch (Exception e) {
		Log.i("Error:  ", e.toString());
	}
	cursor.close();
	mdb.close();
	
}

public void readstoryquestions(){
	Log.d("Table", "StoryQuestions");
	Log.i("TEST", "I am int the reader method readstoryquestions..... ready to start reading");
	String sql="select * from story_questions";
	SQLiteDatabase mdb= this.getReadableDatabase();
	Cursor cursor= mdb.rawQuery(sql, null);
	Log.i("TEST", "Cursor initialized");
	try{
	cursor.moveToFirst();
	while(cursor.moveToNext()){
		int id=cursor.getInt(0);
		String question=cursor.getString(1);
		String marks= cursor.getString(2);
		String answered=cursor.getString(3);
		String story= cursor.getString(4);
		String image= cursor.getString(5);
		String answers=cursor.getString(6);
		Log.i("results:", "ID:"+id+"   question: "+question+"    marks:"+marks+"     answered: "+answered+"    Story:"+story+"     Image:"+image+ " answers: "+answers);
		
	}
	}
	catch (Exception e) {
		Log.i("Error:  ", e.toString());
	}
	cursor.close();
	mdb.close();
	
}

public void readstories(){
	Log.d("Table", "Stories");
	Log.i("TEST", "I am int the reader method readstories..... ready to start reading");
	String sql="select * from stories";
	SQLiteDatabase mdb= this.getReadableDatabase();
	Cursor cursor= mdb.rawQuery(sql, null);
	Log.i("TEST", "Cursor initialized");
	try{
	cursor.moveToFirst();
	while(cursor.moveToNext()){
		int id=cursor.getInt(0);
		String title=cursor.getString(1);
		String story= cursor.getString(2);
		String language=cursor.getString(3);
		String image= cursor.getString(4);
		
		Log.i("results:", "ID:"+id+"   Title: "+title+"    Story:"+story+"     Language: "+language+"    Image:"+image);
		
	}
	}
	catch (Exception e) {
		Log.i("Error:  ", e.toString());
	}
	cursor.close();
	mdb.close();
	
}

public void readscores(){
	Log.d("Table", "Scores");
	Log.i("TEST", "I am int the reader method readscores..... ready to start reading");
	String sql="select * from scores";
	SQLiteDatabase mdb= this.getReadableDatabase();
	Cursor cursor= mdb.rawQuery(sql, null);
	Log.i("TEST", "Cursor initialized");
	try{
	cursor.moveToFirst();
	while(cursor.moveToNext()){
		int id=cursor.getInt(0);
		String scores=cursor.getString(1);
		String user= cursor.getString(2);
				
		Log.i("results:", "ID:"+id+"   Scores: "+scores+"    User: "+user);
		
	}
	}
	catch (Exception e) {
		Log.i("Error:  ", e.toString());
	}
	cursor.close();
	mdb.close();
	
}

public void readquestions(){
	Log.d("Table", "Questions");
	Log.i("TEST", "I am int the reader method readquestions..... ready to start reading");
	String sql="select * from questions";
	SQLiteDatabase mdb= this.getReadableDatabase();
	Cursor cursor= mdb.rawQuery(sql, null);
	Log.i("TEST", "Cursor initialized");
	try{
	cursor.moveToFirst();
	while(cursor.moveToNext()){
		int id=cursor.getInt(0);
		String question=cursor.getString(1);
		String answered= cursor.getString(2);
		String marks=cursor.getString(3);
		String subject= cursor.getString(4);
		String images=cursor.getString(5);
		String answers=cursor.getString(6);
		
		Log.i("results:", "ID:"+id+"   question: "+question+"    answered: "+answered+"     Marks: "+marks+"    Subject: "+subject+"Image: "+images+"Answers: "+ answers);
		
	}
	}
	catch (Exception e) {
		Log.i("Error:  ", e.toString());
	}
	cursor.close();
	mdb.close();
	
}
public void readlanguage(){
	Log.d("Table", "Language");
	Log.i("TEST", "I am int the reader method readlanguages..... ready to start reading");
	String sql="select * from language";
	SQLiteDatabase mdb= this.getReadableDatabase();
	Cursor cursor= mdb.rawQuery(sql, null);
	Log.i("TEST", "Cursor initialized");
	try{
	cursor.moveToFirst();
	while(cursor.moveToNext()){
		int id=cursor.getInt(0);
		String name=cursor.getString(1);
				
		Log.i("results:", "ID:"+id+"   Name: "+name);
		
	}
	}
	catch (Exception e) {
		Log.i("Error:  ", e.toString());
	}
	cursor.close();
	mdb.close();
	
}

public void readimages(){
	Log.d("Table", "Images");
	Log.i("TEST", "I am int the reader method readimages..... ready to start reading");
	String sql="select * from images";
	SQLiteDatabase mdb= this.getReadableDatabase();
	Cursor cursor= mdb.rawQuery(sql, null);
	Log.i("TEST", "Cursor initialized");
	try{
	cursor.moveToFirst();
	while(cursor.moveToNext()){
		int id=cursor.getInt(0);
		String name=cursor.getString(1);
		
		
		Log.i("results:", "ID:"+id+"   Name: "+name);
		
	}
	}
	catch (Exception e) {
		Log.i("Error:  ", e.toString());
	}
	cursor.close();
	mdb.close();
	
}
public void readclasss(){
	Log.d("Table", "Class");
	Log.i("TEST", "I am int the reader method readclasss..... ready to start reading");
	String sql="select * from class";
	SQLiteDatabase mdb= this.getReadableDatabase();
	Cursor cursor= mdb.rawQuery(sql, null);
	Log.i("TEST", "Cursor initialized");
	try{
	cursor.moveToFirst();
	while(cursor.moveToNext()){
		int id=cursor.getInt(0);
		String clss=cursor.getString(1);
		String years= cursor.getString(2);
		
		
		Log.i("results:", "ID:"+id+"   class: "+clss+"    Years:"+years);
		
	}
	}
	catch (Exception e) {
		Log.i("Error:  ", e.toString());
	}
	cursor.close();
	mdb.close();
	
}

public void readchoices(){
	Log.d("Table", "Choices");
	Log.i("TEST", "I am int the reader method readchoices..... ready to start reading");
	String sql="select * from choices";
	SQLiteDatabase mdb= this.getReadableDatabase();
	Cursor cursor= mdb.rawQuery(sql, null);
	Log.i("TEST", "Cursor initialized");
	try{
	cursor.moveToFirst();
	while(cursor.moveToNext()){
		int id=cursor.getInt(0);
		String choice1=cursor.getString(1);
		String choice2= cursor.getString(2);
		String choice3=cursor.getString(3);
		
		
		Log.i("results:", "ID:"+id+"   Choice1: "+choice1+"Choice2: "+choice2+"     Choice3: "+choice3);
		
	}
	}
	catch (Exception e) {
		Log.i("Error:  ", e.toString());
	}
	cursor.close();
	mdb.close();
	
}
public void readanswers(){
	Log.d("Table", "Answers");
	Log.i("TEST", "I am int the reader method readanswers..... ready to start reading");
	String sql="select * from answers";
	SQLiteDatabase mdb= this.getReadableDatabase();
	Cursor cursor= mdb.rawQuery(sql, null);
	Log.i("TEST", "Cursor initialized");
	try{
	cursor.moveToFirst();
	while(cursor.moveToNext()){
		int id=cursor.getInt(0);
		String answer=cursor.getString(1);
		String choice= cursor.getString(2);
		
		
		Log.i("results:", "ID:"+id+"   Answer: "+answer+"   Choice: "+choice);
		
	}
	}
	catch (Exception e) {
		Log.i("Error:  ", e.toString());
	}
	cursor.close();
	mdb.close();
	
}






}
