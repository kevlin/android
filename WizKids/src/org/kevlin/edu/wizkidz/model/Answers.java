package org.kevlin.edu.wizkidz.model;

public class Answers {
	//table columns
	public static String ID="id";
	public static String ANSWERS="answer";
	public static String CHOICE="choice_id";
	
	int id, choice;
	String answer;
	
	//empty constructor
	public Answers(){
		
	}
	
	
	//constructor with all variables
	public Answers(int id, String answer, int choice){
		this.id=id;
		this.answer=answer;
		this.choice=choice;
		
	}
	
	public String createtable(){
		String createtable="CREATE  TABLE IF NOT EXISTS answers (  id INTEGER PRIMARY KEY,  answer TEXT NOT NULL, choice_id INTEGER, FOREIGN KEY (choice_id) REFERENCES choices(id)  );";
		
		return createtable;
	}
	public String droptable(){
		String droptable="DROP TABLE IF EXISTS answers ;";
		
		return droptable;
	}
	
	
	
	//getters and setters goes here
	
	public int getId(){
		return this.id;
	}
	public void setId(int id){
		this.id=id;
	}
	
	
	
	public String getAnswer(){
		return this.answer;
	}
public void setAnswer(String answer){
	this.answer=answer;
}



public int getChoice(){
	return this.choice;
}
public void setChoice(int choice){
	this.choice=choice;
}
}
