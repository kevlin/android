package org.kevlin.edu.wizkidz.model;

public class Choices {
	public static String ID="id";
	public static String CHOICE1="choice1";
	public static String CHOICE2="choice2";
	public static String CHOICE3="choice3";
	
	
	
	int id;
	String choice1,choice2,choice3;
	
	//empty constructor
	 public Choices() {
		
		 
	}	
	
	
	//constructor with all variables
	public Choices(int id, String choice1,String choice2, String choice3){
		this.id=id;
		this.choice1=choice1;
		this.choice2=choice2;
		this.choice3=choice3;
		
	}
	
	public String createtable(){
		String createtable="CREATE  TABLE IF NOT EXISTS choices (  id INTEGER PRIMARY KEY,  choice1 TEXT , choice2 TEXT , choice3 TEXT );";
		
		return createtable;
	}
	public String droptable(){
		String droptable="DROP TABLE IF EXISTS choices ;";
		
		return droptable;
	}
	
	
	
	//getters and setters goes here
	
	public int getId(){
		return this.id;
	}
	public void setId(int id){
		this.id=id;
	}
	
	
	
	public String getChoice1(){
		return this.choice1;
	}
public void setChoice1(String choice1){
	this.choice1=choice1;
}



public String getChoice2(){
	return this.choice2;
}
public void setChoice2(String choice2){
this.choice2=choice2;
}



public String getChoice3(){
	return this.choice3;
}
public void setChoice3(String choice3){
this.choice3=choice3;
}
}
