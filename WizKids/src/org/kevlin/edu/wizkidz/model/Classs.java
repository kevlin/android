
//Author Kevin Kaburu


package org.kevlin.edu.wizkidz.model;

public class Classs {
	//table columns
	public static String ID="id";
	public static String CLASS ="class";
	public static String YEARS="years";
	
	int id,years;
	String classs;
	
	//Empty constructor
	public Classs(){
		
	}
	
	//constructor with all variables
	public Classs(int id,String classs,int years){
		this.id=id;
		this.classs=classs;
		this.years=years;
	}
	//constructor without the id
	
	public Classs(String classs,int years){
		this.classs=classs;
		this.years=years;
	}
	
	public String createtable(){
		String creattable="CREATE  TABLE IF NOT EXISTS class (   id INTEGER PRIMARY KEY ,  class TEXT NOT NULL ,  years INTEGER NOT NULL   );";
		
		return creattable;
	}
	//this is the method that will be used to return a drop table query.
	public String droptable(){
		
		String droptable="DROP TABLE IF EXISTS class ;";
		
		return droptable;
	}
	
	
	//getters and setters
	
	public int getId(){
		return this.id;
	}
	public void setId(int id){
		this.id=id;
	}
	
	
	
	public String getClasss(){
		return this.classs;
	}
	public void setClasss(String classs){
		this.classs=classs;
	}
	
	
	
	public int getYears(){
		return this.years;
	}
	public void setYears(int years){
		this.years=years;
	}

}
