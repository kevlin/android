package org.kevlin.edu.wizkidz.model;

public class Images {
	//table columns
	public static String ID="id";
	public static String NAME="name";
	
int id;
String name;
	
	//Empty constructor
	public Images(){
		
	}
	
	
	//constructor with all variables
	public Images(int id, String name){
		this.id=id;
		this.name=name;
		
	}
	
	
	
	public String createtable (){
		
		String createtable="CREATE  TABLE IF NOT EXISTS images (  id INTEGER PRIMARY KEY ,  name TEXT NOT NULL  );";
		
		
		return createtable;
	}
	public String droptable(){
		 String droptable="DROP TABLE IF EXISTS images ;";
		
		
		return droptable;
	}
	
	
	//getter and setter for id
	public int getID(){
		return this.id;
	}
	public void setID(int id){
		this.id=id;
		
	}
	
	//getter and setter for name
	public String getName(){
		return this.name;
	}
	public void setName(String name){
		this.name=name;
		
	}

}
