package org.kevlin.edu.wizkidz.model;

public class Language {
	//these are the table columns
	public static String ID="id";
	public static String NAME="name";
	
	int id;
	String name;
	
	//empty constructor
	public Language(){
		
	}
	//constructor with all variables
	public Language(int id, String name){
		this.id=id;
		this.name=name;
		
	}
	
	public String createtable(){
		String createtable="CREATE  TABLE IF NOT EXISTS language (  id INTEGER PRIMARY KEY,  name TEXT NOT NULL );";
		
		return createtable;
	}
	
	// A method to return the sql statement to be used to drop the table
	
	public String droptable(){
		String droptable="DROP TABLE IF EXISTS language ;";
		
		
		return droptable;
	}
	//getters and setters goes here
	public int getId(){
		return this.id;
	}
	public void setId(int id){
		this.id=id;
	}

	
	
	
	
	public String getName(){
		return this.name;
	}
	public void setName(String name){
		this.name=name;
	}
}
