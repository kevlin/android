package org.kevlin.edu.wizkidz.model;

public class Questions {
	//these are the available columns
	public static String ID="id";
	public static String QUESTIONS="question";
	public static String ANSWERED="answered";
	public static String MARKS="marks";
	public static String SUBJECTID="subject_id";
	public static String IMAGEID="image_id";
	public static String ANSWERID="answer_id";
	// variables to hold data to and from the table
	int id,marks,subject,image,answer,answered;
	String question;
	//empty constructor
	public Questions(){
		
	}
	
	
	//constructor with all variables
	public Questions(int id,String questions,int answered,int marks,int subject,int image,int answer){
		this.id=id;
		this.question=questions;
		this.answered=answered;
		this.marks=marks;
		this.subject=subject;
		this.image=image;
		this.answer=answer;
	}

	//constructor without the id
	public Questions(String questions,int answered,int marks,int subject,int image,int answer){
		this.question=questions;
		this.answered=answered;
		this.marks=marks;
		this.subject=subject;
		this.image=image;
		this.answer=answer;
	}
	
	//this method will return the sql statement to be used to create the table
	public String createtable(){
		String createtable ="CREATE  TABLE IF NOT EXISTS questions (  id INTEGER PRIMARY KEY ,  question TEXT NOT NULL ,  answered INTEGER NOT NULL ,  marks INTEGER NOT NULL ,  subjects_id INTEGER NOT NULL ,  images_id INTEGER ,  answers_id INTEGER NOT NULL, FOREIGN KEY (subjects_id )    REFERENCES subjects (id ),  FOREIGN KEY (images_id )    REFERENCES images (id ), FOREIGN KEY (answers_id )    REFERENCES answers (id ));";
		
		
		
		return createtable;
	}
	public String droptable(){
		String droptable="DROP TABLE IF EXISTS questions ;";
		
		
		return droptable;
	}
	//Generate the getters and the setters
	
	public int getId(){
		return this.id;
	}
	public void setId(int id){
		this.id=id;
	}
	
	
	public String getQuestion(){
		return this.question;
	}
	public void setQuestion(String question){
		this.question=question;
	}
	
	
	public int getAnswered(){
		return this.answered;
	}
	public void setAnswered(int answered){
		this.answered=answered;
	}
	
	
	public int getMarks(){
		return this.marks;
	}
	public void setMarks(int marks){
		this.marks=marks;
	}
	
	
	public int getSubject(){
		return this.subject;
	}
	public void setSubject(int subject){
		this.subject=subject;
	}
	
	
	public int getImage(){
		return this.image;
	}
	public void setImage(int image){
		this.image=image;
	}
	
	
	public int getAnswer(){
		return this.answer;
	}
	public void setAnswer(int answer){
		this.answer=answer;
	}
	
	
}
