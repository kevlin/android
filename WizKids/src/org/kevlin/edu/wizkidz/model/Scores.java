package org.kevlin.edu.wizkidz.model;

public class Scores {
	//these are the columns in that table
	public static String ID="id";
	public static String SCORES="scores";
	public static String USERID="user_id";
	
	int id,scores,user;
	
	//empty constructor
	public Scores(){
		
	}
	//constructor with all the variables
	public Scores(int id, int scores, int user){
		this.id=id;
		this.scores=scores;
		this.user=user;
	}
	//constructor without the id
	public Scores(int scores,int user){
		this.scores=scores;
		this.user=user;
	}
	
	public String createtable(){
		String createtable=" CREATE  TABLE IF NOT EXISTS scores (  id INTEGER PRIMARY KEY,  scores INTEGER NOT NULL ,  user_id INTEGER NOT NULL ,   FOREIGN KEY (user_id )    REFERENCES user (id ) );";
		
		
		return createtable;
	}
	public String droptable(){
		String droptable="DROP TABLE IF EXISTS scores ;";
		
		
		return droptable;
	}
	//here goes the getters and the setters
	
	public int getId(){
		return this.id;
	}
	public void setId(int id){
		this.id=id;
	}
	
	
	public int getScores(){
		return this.scores;
	}
	public void setScores(int scores){
		this.scores=scores;
	}
	public int getUser(){
		return this.user;
	}
	public void setUser(int user){
		this.user=user;
	}

}
