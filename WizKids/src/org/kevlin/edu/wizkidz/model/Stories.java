package org.kevlin.edu.wizkidz.model;

public class Stories {
	//this are the coluns in the table
	public static String COL_ID="id";
	public static String COL_TITLE="title";
	public static String COL_STORY="story";
	public static String COL_LANGUAGE="language_id";
	public static String COL_IMAGE="image_id";
	public static String TABLE="stories";
	
	int id,lang,image;
	String title,story;
	
	
	//empty constructor
	public Stories(){
		
	}
	
	//constructor with all data
	public Stories(int id,String title,String story,int language,int image){
		this.id=id;
		this.title=title;
		this.story=story;
		this.lang=language;
		this.image=image;
		
	}
	//constructor without the id
	public Stories(String title,String story,int language,int image){
		this.title=title;
		this.story=story;
		this.lang=language;
		this.image=image;
	}
	//string to be used to create the table Stories
	public String createtable(){
		String createtable="CREATE  TABLE IF NOT EXISTS stories (  id INTEGER PRIMARY KEY ,  title TEXT NOT NULL ,  story TEXT NOT NULL,  language_id INTEGER NOT NULL,  images_id INTEGER  ,   FOREIGN KEY (language_id )    REFERENCES language (id),  FOREIGN KEY (images_id )    REFERENCES images (id ) );";
		return createtable;
	}
	
	public String droptable(){
		String droptable="DROP TABLE IF EXISTS stories ;";
		return droptable;
		
			}
	
	
	//getter and setter for id
	public int getID(){
		return this.id;
	}
	public void setID(int id){
		this.id=id;
		
	}
	
	//getter and setter for title
	
	public String getTitle(){
		return this.title;
	}
	public void setTitle(String title){
		this.title=title;
	}
	
	
	//getter and settter for story
	
	public String getStory(){
		return this.story;
	}
	public void setStory(String story){
		this.story=story;
	}
	
	//getter and setter for language
	public int getLang(){
		return this.lang;
	}
	public void setLang(int lang){
		this.lang=lang;
		
	}
	
	
	//getter and setter for image
	
	public int getImage(){
		return this.image;
	}
	public void setImage(int image){
		this.image=image;
		
	}
	

}
