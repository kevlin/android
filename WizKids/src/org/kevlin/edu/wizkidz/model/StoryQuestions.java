package org.kevlin.edu.wizkidz.model;

public class StoryQuestions {
	//columns in the table
	public static String ID="id";
	public static String QUESTION="question";
	public static String MARKS="marks";
	public static String ANSWERED="answered";
	public static String STORY_ID="story_id";
	public static String IMAGE_ID="image_id";
	public static String ANSWER_ID="answer_id";
	//variables 
	int id, marks,answered,story,image,answer;
	String question;
	//Empty constructor
	public StoryQuestions(){
		
	}
	//constructor initializing all the variables
	public StoryQuestions(int id, String question,int marks,int answered, int story, int image, int answer){
		this.id=id;
		this.question=question;
		this.marks=marks;
		this.answered=answered;
		this.story=story;
		this.image=image;
		this.answer=answer;
	}
	
	//constructor with all the variables minus id
	public StoryQuestions(String question,int marks,int answered, int story, int image, int answer){
		this.question=question;
		this.marks=marks;
		this.answered=answered;
		this.story=story;
		this.image=image;
		this.answer=answer;
	}
	
	public String createtable(){
		
		String createtable="CREATE  TABLE IF NOT EXISTS story_questions (  id  INTEGER PRIMARY KEY,  question TEXT NOT NULL ,  marks INTEGER NOT NULL ,  answered INTEGER NOT NULL ,  stories_id INTEGER NOT NULL ,  images_id INTEGER  ,  answers_id INTEGER NOT NULL ,    FOREIGN KEY (stories_id )    REFERENCES stories (id )  ,FOREIGN KEY (images_id )    REFERENCES images (id ) ,  FOREIGN KEY (answers_id )    REFERENCES answers (id ) );";
		
		
		
		
		return createtable;
	}
	public String droptable(){
		String droptable="DROP TABLE IF EXISTS story_questions;";
		
		return droptable;
		
	}
	//getters and setters
	//Generate the getters and the setters
	
		public int getId(){
			return this.id;
		}
		public void setId(int id){
			this.id=id;
		}
		
		
		public String getQuestion(){
			return this.question;
		}
		public void setQuestion(String question){
			this.question=question;
		}
		
		
		public int getAnswered(){
			return this.answered;
		}
		public void setAnswered(int answered){
			this.answered=answered;
		}
		
		
		public int getMarks(){
			return this.marks;
		}
		public void setMarks(int marks){
			this.marks=marks;
		}
		
		
		public int getStory(){
			return this.story;
		}
		public void setSubject(int story){
			this.story=story;
		}
		
		
		public int getImage(){
			return this.image;
		}
		public void setImage(int image){
			this.image=image;
		}
		
		
		public int getAnswer(){
			return this.answer;
		}
		public void setAnswer(int answer){
			this.answer=answer;
		}

}
