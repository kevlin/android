package org.kevlin.edu.wizkidz.model;

public class Subjects {
	//these are the table column names
	public static String ID="id";
	public static String SUBJECT="subject";
	public static String CLASSID="class_id";
	int id,classs;
	String subject;
	//empty constructor
	public Subjects(){
		
	}
	//constructor with all variables initiated
	public Subjects(int id, String subject, int classs){
		this.id=id;
		this.subject=subject;
		this.classs=classs;
		
	}
	//constructor without id
	public Subjects(String subject, int classs){
		this.subject=subject;
		this.classs=classs;
	}
	
	
	public String createtable(){
		String createtable="CREATE  TABLE IF NOT EXISTS subjects (  id INTEGER  PRIMARY KEY ,  subject TEXT NOT NULL ,  class_id  INTEGER ,  FOREIGN KEY (class_id )    REFERENCES class (id ) );";
		
		return createtable;
	}
	
	public String droptable(){
		String droptable="DROP TABLE IF EXISTS subjects ;";
		
		
		return droptable;
	}
	
	//defiing the getters and the setters
	public int getId(){
		return this.id;
	}
	public void setId(int id){
		this.id=id;
	}
	
	
	public String getSubject(){
		return this.subject;
	}
	public void setSubject(String subject){
		this.subject=subject;
	}
	
	public int getClasss(){
		return this.classs;
	}
	public void setClasss(int classs){
		this.classs=classs;
	}
		
}
