package org.kevlin.edu.wizkidz.model;

import java.util.Date;

public class User {
	//list of all columns in the table
	public static String ID="id";
public static String FNAME="fname";
public static String LNAME="lname";
public static String DOB="dob";
public static String SCHOOL="school";
public static String CLASS="class";//this is a foreign key

int id;
String dob;
String fname,lname,school;
String classs;
//empty constructor
public User(){
	
}
//constructor with all the variables initialized
public User(int id, String dob,String fname,String lname,String school,String classs){
	this.id=id;
	this.dob=dob;
	this.fname=fname;
	this.lname=lname;
	this.school=school;
	this.classs=classs;
	
	
	
}

//construct without the id
public User(String dob,String fname,String lname,String school,String classs){
	this.dob=dob;
	this.fname=fname;
	this.lname=lname;
	this.school=school;
	this.classs=classs;	
}


public String createtable(){
	String createtable="CREATE  TABLE IF NOT EXISTS user (  id INTEGER PRIMARY KEY ,  fname TEXT NOT NULL ,  lname TEXT NOT NULL ,  dob TEXT NOT NULL ,  school TEXT NOT NULL ,  class TEXT NOT NULL );";
	
	
	return createtable;
}
//a method to return the sql statement to drop the table

public String droptable(){
	String droptable="DROP TABLE IF EXISTS user ;";
	
	
	return droptable;
}



//getters and setters
public int getId(){
	return this.id;
}
public void setId(int id){
	this.id=id;
}


public String getDob(){
	return this.dob;
}
public void setDob(String dob){
	this.dob=dob;
}

public String getFname(){
	return this.fname;
}
public void setFname(String fname){
	this.fname=fname;
}


public String getLname(){
	return this.lname;
}
public void setLname(String lname){
	this.lname=lname;
}

public String getSchool(){
	return this.school;
}
public void setSchool(String school){
	this.school=school;
}

public String  getClasss(){
	return this.classs;
}
public void setClasss(String classs){
	this.classs=classs;
}
}
